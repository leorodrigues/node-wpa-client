
/**
 * This interface describes the component needed to inspect the data string
 * received from the `wpa_supplicant` and produce an event name and
 * an event data.
 */
export declare interface IEventParser {
    /**
     * Implementations of this mehod should parse the given data string
     * and return the event name.
     * @param message A data string.
     * @returns The event name.
     */
    parseName(message: string): string;

    /**
     * Implementations of this method should parse the given data string
     * and return the event data.
     * @param message A data string.
     * @returns The event data object.
     */
    parseData(message: string): any;
}

/**
 * This interface describes the operations needed to control a given socket.
 */
export declare interface ISocketLifeCycle {

    /**
     * The implementations of this method should open a connection
     * to the `wpa_supplicant` daemon via an interface socket file.
     * @param serverPath A string pointing to the `wpa_supplicant` interface
     * on disk.
     * @returns A void promise.
     */
    connect(serverPath: string): Promise<void>;

    /**
     * The implementatioins of this method should bind a socket on disk, to
     * be used by the `wpa_supplicant` to send back its messages.
     * @param localPath A string pointing to a location on disk
     * where the control socket will be linked.
     * @returns A void promise.
     */
    bind(localPath: string): Promise<void>;

    /**
     * The implementations of this method should close all communication
     * mechanics with the `wpa_supplicant` and release all related resources.
     * @returns A void promise.
     */
    close(): Promise<void>;
}

/**
 * This interface describes the event handling functions signature
 * used by the `IEventReceiver`.
 */
export declare interface IEventReceiverCallback { (...args: any[]): void; }

/**
 * Classes implementing this interface are declaring that they serve as sources
 * of events comming from the `wpa_supplicant`.
 */
export declare interface IEventReceiver {
    /**
     * This method signals the implementation of `IEventReceiver` to start
     * piping events through from the underlying communication engines.
     * It is supposed to be called AFTER all interesting events have been
     * bound to their respective handlers by this interfaces consumer.
     */
    standByForEvents(): void;

    /**
     * Implementations of this method should execute the handler callback
     * whenever the given event happens.
     * @param event A string or symbol identifying the event.
     * @param callback A handler function.
     * @returns This method should return nothing.
     */
    on(event: string | Symbol, callback: IEventReceiverCallback): void;

    /**
     * Implementations of this method should execute the handler only at the
     * first occurrence of the given event.
     * @param event A string or symbol identifying the event.
     * @param callback A handler function.
     * @returns This method should return nothing.
     */
    once(event: string | Symbol, callback: IEventReceiverCallback): void;
}

/**
 * Classes implementing this interface are declaring that they serve as sources
 * of messages in response to commands previously transmitted
 * to the `wpa_supplicant` daemon.
 * @see Communication/IMessageTransmitter
 */
export declare interface IMessageReceiver {

    /**
     * Implementations of this method should return the most recent reply from
     * the `wpa_supplicant` daemon.
     * @returns A promise to the reply from the daemon as a string.
     */
    receive(): Promise<string>;
}

/**
 * Classes implementing this interface are declaring that they are capable of
 * transmitting commands to the `wpa_supplicant` daemon.
 * @see Communication/IMessageReceiver
 */
export declare interface IMessageTransmitter {

    /**
     * Implementations of this method should send the string payload
     * to the `wpa_supplicant`.
     * @param message The command string to `wpa_supplicant`.
     * @returns A void promise.
     */
    transmit(message: string): Promise<void>;
}

/**
 * Classes implementing this interface are declaring that they work as both
 * transmitter and receiver in a coordinated manner, i.e. for each command
 * sent, there will be a reply received shortly after. Bear in mind tho, that
 * the actual coordination of calls to `transmit` and `receive` are executed by
 * the consumer of this interface and the implementation should do its best
 * to guarantee, that each response corresponds to its command.
 */
export declare interface IMessageTransceiver
    extends IMessageReceiver, IMessageTransmitter { }

/**
 * Classes implementing this interface are declaring that they work as both
 * transmitter and event receiver. Those classes should provide a framework
 * in which the commands transmitted to the `wpa_supplicant` don't have a direct
 * response. Instead they will trigger one or more events that should be handled
 * asynchronously.
 */
export declare interface IEventTransceiver
    extends IEventReceiver, IMessageTransmitter { }

/**
 * This class is built to work with the `wpa_supplicant` daemon
 * in an event oriented manner. It is build as a facade that delegates
 * its calls to smaller specialized components.
 */
export declare class MonitorTransceiver
    implements IEventTransceiver, ISocketLifeCycle {

    /**
     * It instantiates a new `MonitorTransceiver`.
     * @param eventReceiver An `IEventReceiver` delegate.
     * @param messageTransmitter An `IMessageTransmitter` delegate.
     * @param socketLifeCycle An `ISocketLifeCycle` delegate.
     */
    constructor(
        eventReceiver: IEventReceiver,
        messageTransmitter: IMessageTransmitter,
        socketLifeCycle: ISocketLifeCycle);

    /**
     * It signals the monitor to start "pumping" events through. Should only
     * be called AFTER all events are bound to their respective handlers.
     */
    standByForEvents(): void;

    /**
     * It binds the given callback to an event. Every time the event occurs,
     * the callback will be run.
     * @param event A string with the event name or identifier.
     * @param callback A handler function.
     */
    on(event: string | Symbol, callback: IEventReceiverCallback): void;

    /**
     * It binds the given callback to an event. The callback will answer
     * ONLY the next occurrence of the event and will no longer be used.
     * @param event A string with the event name or identifier.
     * @param callback A handler function.
     */
    once(event: string | Symbol, callback: IEventReceiverCallback): void;

    /**
     * It sends commands to the `wpa_supplicant` daemon. Responses are supposed
     * to come back through the event handlers.
     * @param message A command string to be sent to the `wpa_supplicant`.
     * @returns A void promise.
     */
    transmit(message: string): Promise<void>;

    /**
     * It connects the monitor instance to the `wpa_supplicant` daemon.
     * @param serverPath The path to the `wpa_supplicant` interface socket file.
     * @returns A void promise.
     */
    connect(serverPath: string): Promise<void>;

    /**
     * It will make the monitor listen for messages on a control socket.
     * The events from the `wpa_supplicant` daemon will be received through
     * this control socket.
     * @param localPath A string pointing to the soon-to-be-created control
     * socket file.
     * @returns A void promise.
     */
    bind(localPath: string): Promise<void>;

    /**
     * It relinquishes the connection with the `wpa_supplicant` interface
     * and the control socket.
     * @returns A void promise.
     */
    close(): Promise<void>;
}

/**
 * This class is built to work with the `wpa_supplicant` daemon
 * in an request/response manner. The consumer is actually responsible for
 * this coordination, having to call `transmit` and then `receive`.
 * The transceiver will do its best to maintain request/response coorelation
 * between calls.
 */
export declare class CommandTransceiver
    implements IMessageTransceiver, ISocketLifeCycle {

    /**
     * It instantiates a new `CommandTransceiver`.
     * @param messageReceiver An `IMessageReceiver` delegate.
     * @param messageTransmitter An `IMessageTransmitter` delegate.
     * @param socketLifeCycle An `ISocketLifeCycle` delegate.
     */
    constructor(
        messageReceiver: IMessageReceiver,
        messageTransmitter: IMessageTransmitter,
        socketLifeCycle: ISocketLifeCycle);

    /**
     * It will receive the next available reply
     * from the `wpa_supplicant` daemon.
     * @returns A promise to a string.
     */
    receive(): Promise<string>;

    /**
     * It transmits a command to the `wpa_supplicant` daemon. The responses
     * are supposed to be retrieved through calls to the `receive` method.
     * @param message A command string.
     * @returns A void promise.
     */
    transmit(message: string): Promise<void>;

    /**
     * It connects the transceiver instance to the `wpa_supplicant` daemon.
     * @param serverPath The path to the `wpa_supplicant` interface socket file.
     * @returns A void promise.
     */
    connect(serverPath: string): Promise<void>;

    /**
     * It will make the transceiver listen for messages on a control socket.
     * The replies from the `wpa_supplicant` daemon will be received through
     * this control socket.
     * @param localPath A string pointing to the soon-to-be-created control
     * socket file.
     * @returns A void promise.
     */
    bind(localPath: string): Promise<void>;

    /**
     * It relinquishes the connection with the `wpa_supplicant` interface
     * and the control socket.
     * @returns A void promise.
     */
    close(): Promise<void>;
}

/**
 * This class implements the control operations around a given socket.
 */
export declare class SocketLifeCycle implements ISocketLifeCycle {

    /**
     * Instantiates a new `SocketLifeCycle`.
     * @param socket An abstraction of a socket.
     */
    constructor(socket: any);

    /**
     * It establishes a clinet-server connection from the given socket to
     * the `wpa_supplicant` daemon.
     * @param serverPath A string with the path to one of the `wpa_supplicant`
     * interface socket files.
     * @returns A void promise.
     */
    connect(serverPath: string): Promise<void>;

    /**
     * It establishes a way for the `wpa_supplicant` daemon send back
     * the replies for the given commands.
     * @param localPath A string to the soon-to-be-created control socket file.
     * @returns A void promise.
     */
    bind(localPath: string): Promise<void>;

    /**
     * It will relinquish the socket files and free up the allocated resources.
     * @returns A void promise.
     */
    close(): Promise<void>;
}

/**
 * This class will receive the events from `wpa_supplicant` and transform
 * them to more specific abstractions and events.
 */
export declare class EventReceiver implements IEventReceiver {

    /**
     * It creates a new instance of `EventReceiver`.
     * @param socket An abstract socket instance.
     * @param eventParser A delegate that will actually produce
     * the higher level abstractions.
     * @param options Options to configure the event emitter.
     */
    constructor(socket: any, eventParser: IEventParser, options: any);

    /**
     * It makes the instance bind itself to the socket and start processing
     * the event messages as soon as they arrive.
     */
    standByForEvents(): void;

    /**
     * It fires the handler function whenever the event
     * with the given name occurs.
     * @param event A string or symbol identifying a `wpa_supplicant` event.
     * @param callback A handler function.
     */
    on(event: string | Symbol, callback: IEventReceiverCallback);

    /**
     * It fires the handler function only once, the next time the event
     * with the given name occurs.
     * @param event A string or symbol identifying a `wpa_supplicant` event.
     * @param callback A handler function.
     */
    once(event: string | Symbol, callback: IEventReceiverCallback);
}

/**
 * This class will take messages from the given socket and make them available
 * to the caller. It uses a pair of internal queues to coordinate the arrival
 * of events from `wpa_supplicant` and the `receive` calls.
 */
export declare class MessageReceiver implements IMessageReceiver {
    /**
     * It creates a new instance of `MessageReceiver`.
     * @param socket An abstract socket instance.
     */
    constructor(socket: any);

    /**
     * It will return the next outstanding message from `wpa_supplicant`.
     * It means that if a message has been received and queued, that message
     * will be returned. On the other hand, if the outstanding queue is empty,
     * the `receive` call will be placed on an "outstanding resolutions" queue.
     * As soon as a message arrives, the next outstanding resolution will be
     * fullfilled.
     * @returns A promise to the next data string.
     */
    receive(): Promise<string>;
}

/**
 * This class will send command messages to the `wpa_supplicant` daemon.
 */
export declare class MessageTransmitter implements IMessageTransmitter {
    /**
     * It creates a new instance of `MessageTransmitter`.
     * @param socket An abstract socket instance.
     */
    constructor(socket: any);

    /**
     * It will send the command to the server and resolve
     * the promise immediately.
     * @param message A command message string.
     * @returns A void promise.
     */
    transmit(message: string): Promise<void>;
}