// Type definitions for wpa-client 0.0.1-alpha
// Project: wpa-client
// Definitions by: Leonardo Rodrigues Teixeira <leorodriguesrj@gmail.com>

import * as Communication from './communication';

import * as Commands from './commands';

export { Communication, Commands };

/**
 * This class is responsible for analysing a raw data string returned by the
 * `wpa_supplicant`. It provides methods to retrieve the event name and the
 * event data from a given raw string.
 */
export declare class WpaEventParser implements Communication.IEventParser {
    /**
     * Get the name of the event from the raw data string
     * @param data A raw data string
     */
    parseName(data: string): string;

    /**
     * Get the data of the event from the raw data string
     * @param data A raw data string
     */
    parseData(data: string): any;
}

/**
 * This class abstracts the locations of the socket files. It is used to keep
 * the deeper components of the library isolated from the algorithms that
 * combine path elements and deal with file system details.
 */
export declare class PathResolver {
    /**
     * Creates a new instance of `PathResolver`.
     * @param interfaceName The name of one of the `wpa_supplicant`s interfaces.
     * @param controlId An optional unique string to identify
     * the soon-to-be-created control socket file.
     */
    constructor(interfaceName: string, controlId?: string);

    /**
     * This method will compute the path to the server socket file. It may
     * throw an exception if the `fs.accessSync(computedPath)` fails.
     * @returns The path to the server socket file in the file system.
     */
    getServerSocketPath(): string;

    /**
     * This method will compute a path to the control socket file. Such file
     * must not exist and the call to `fs.accessSync(fileDir)` must succeed.
     * 
     * The optional argument allows you to compute variations of the control
     * socket file based on a per-file id. A simple integer number is suggested
     * and the default value is "0". Bellow we have the file pattern
     * and an example:
     * 
     * - Pattern: /tmp/wpa_ctrl_{controlId}-{fileId}.sock
     * - Example: /tmp/wpa_ctrl_112358-0.sock
     * 
     * @param fileId An optional string. Default is "0".
     * @returns The path to the client control socket in the file system.
     */
    getControlSocketPath(fileId?: string): string;
}

/**
 * This callback function is called to configure the registrar. It should
 * use the registrar's binding functions to provide handlers to the various
 * events sent by the monitor transceiver.
 */
export declare interface ConfigureRegistrar {
    /**
     * @param registrar The registrar that should be configured.
     * @param client The instance of `WpaClient` that invoked
     * the configuration callback.
     */
    (registrar: Commands.EventRegistrar, client: WpaClient): void;
}

/**
 * This class implements the `Facade` pattern. It exposes all the possible
 * operations provided by the library while hiding the internal complexities
 * and intricancies of the communication between its components.
 * 
 * Some of the operations will return results right after promise resolution.
 * Others will trigger sequences of events that will be fired by the
 * `wpa_supplicant` daemon.
 * 
 * @see {@link http://w1.fi/wpa_supplicant/devel/ctrl_iface_page.html} For
 * details of the underlying message structures.
 */
export declare class WpaClient {
    /**
     * It instantiates a new `WpaClient`.
     * @param pathResolver An instance of `PathResolver`.
     * @param commandKit An instance of `CommandKit`.
     */
    constructor(pathResolver: PathResolver, commandKit: Commands.CommandKit);

    /**
     * It will open the communication sockets with the `wpa_supplicant` daemon.
     * @returns A void promise.
     */
    open(): Promise<void>;

    /**
     * It will close the sockets currently opened.
     * @returns A void promise.
     */
    close(): Promise<void>;

    /**
     * It configures the client to handle the `wpa_supplicant` events.
     * @param configure A callback function.
     */
    standByForEvents(configure: ConfigureRegistrar): void;

    /**
     * It verifies if the `wpa_supplicant` instance is responding.
     * 
     * It invokes the underlying command `PING`.
     *
     * @returns A promise that returns the string "PONG".
     */
    ping(): Promise<boolean>;

    /**
     * It lists all configured interfaces. Each interface name is a single item
     * in the returned array.
     * 
     * It invokes the underlying command `INTERFACES`.
     * 
     * @returns A promise to an array of strings.
     */
    listInterfaces(): Promise<string[]>;

    /**
     * It will scan for networks during a given amount of time. The scan result
     * will be returned on a list.
     * 
     * Under the hood, this method will wait for the given period, between
     * transmitting the command and receiving the response from the
     * `wpa_supplicant` daemon. If, however, the period is `Period.UNKNOWN`,
     * there will be no watting between the transmit and receive calls.
     * 
     * It orchestrates calls to two underlying commands:
     * `SCAN` and `SCAN_RESULTS`.
     * 
     * @param period Timeout period.
     * @returns A promise to an array of network records.
     */
    scan(period: Commands.Period): Promise<Commands.ScanResult[]>;

    /**
     * It performs multiple calls to the `wpa_supplicant` to create
     * and configure a given network. The given network record must be
     * partially filled with data to be set in the underlying system.
     * Only the defined properties will be used.
     * 
     * It orchestrates calls to two underlying commands:
     * `ADD_NETWORK` and `SET_NETWORK`.
     * 
     * @param network A configuration record for the new network.
     * @returns A promise for the network id.
     */
    addNetworkEntry(network: Commands.NetworkInfo): Promise<number>;

    /**
     * It performs multiple calls to the `wpa_supplicant` to reconfigure a
     * given network. The given network record must be partially filled with
     * data to be replaced in the underlying system. Only the defined
     * properties will be used.
     * 
     * It orchestrates multiple calls to the underlying `SET_NETWORK` command.
     * 
     * @param networkInfo A partially filled `NetworkInfo`.
     * @returns A void promise.
     */
    patchNetworkEntry(networkInfo: Commands.NetworkInfo): Promise<void>;

    /**
     * It lists the networks currently configured within the
     * `wpa_supplicant` daemon.
     * 
     * @returns A promise for an array of network configuration records.
     */
    listNetworkEntries(): Promise<Commands.ListResult[]>;

    /**
     * It requests the current WPA/EAPOL/EAP status information.
     * 
     * It invokes the underlying operation `STATUS`.
     * 
     * @returns A promise for the status information.
     */
    status(): Promise<Commands.StatusResult>;

    /**
     * It enables the network with the given reference (as returned
     * by the `addNetworkEntry` method). There is also the special reference
     * `NetworkEntryReference.ALL` that can be used to enable all networks
     * entries.
     * 
     * It calls the underlying command `ENABLE_NETWORK`.
     * 
     * @param networkEntryReference The network id.
     * @returns A void promise.
     */
    enableNetworkEntry(networkEntryReference: Commands.NetworkEntryReference): Promise<void>;

    /**
     * It disables the network with the given id. (as returned
     * by the `addNetworkEntry` method). There is also the special id `all`
     * that can be used to disable all networks.
     * 
     * It calls the underlying command `DISABLE_NETWORK`.
     * 
     * @param networkEntryReference The network id.
     * @returns A void promise.
     */
    disableNetwork(networkEntryReference: Commands.NetworkEntryReference): Promise<void>;

    /**
     * It enables a single network by id (like the one outputed by `addNetworkEntry`)
     * and disables the others.
     * 
     * It calls the underlying command `SELECT_NETWORK`.
     * 
     * @param networkEntryReference The network id.
     * @returns A void promise.
     */
    selectNetworkEntry(networkEntryReference: Commands.NetworkEntryReference): Promise<void>;

    /**
     * It disconnects the interface bound to the command socket. The interface
     * will remain disconnected unless you call `reassociate` or `reconnect`.
     * 
     * It calls the underlying command `DISCONNECT`.
     * 
     * @returns A void promise.
     */
    disconnect(): Promise<void>;

    /**
     * It forces the reassociation of the interface bound to the command socket.
     * 
     * It calls the underlying command `REASSOCIATE`.
     * 
     * @returns A void promise.
     */
    reassociate(): Promise<void>;

    /**
     * It connects the interface bound to the command socket, but only
     * if the interface is currently disconnected.
     * 
     * It calls the underlying command `RECONNECT`.
     * 
     * @returns A void promise.
     */
    reconnect(): Promise<void>;

    /**
     * It runs a P2P discovery for a given period or until terminated
     * by `p2pStop` or `p2pConnect`.
     * 
     * It calls the underlying command `P2P_FIND`.
     * 
     * @param period Amount of time that the search will run for.
     * @param type Enum of possible find algorithms.
     * @returns A void promise.
     */
    p2pFind(period: Commands.Period, type: Commands.P2PFindType): Promise<void>;

    /**
     * It stops device discovery or other operations (i.e. connect, listen).
     * 
     * It calls the underlying command `P2P_STOP_FIND`.
     * 
     * @returns A void promise.
     */
    p2pStop(): Promise<void>;

    /**
     * It starts a P2P group formation with a discovered peer. The process
     * will perform owner negotiation, group interface setup, provisioning
     * and establishing data connection.
     * 
     * It calls the underlying command `P2P_CONNECT`.
     * 
     * @param peerConnectionParams A DTO instance with
     * the necessary connection parameters.
     * @returns A void promise.
     */
    p2pConnect(peerConnectionParams: Commands.PeerConnectionParams):
        Promise<void>;

    /**
     * It fetches information about a peer. Given you have executed the
     * `p2pFind` method, you may pass in a `PeerReference` instance.
     * 
     * This method uses the underlying command `P2P_PEER`.
     * 
     * @param peerReference A reference to a peer on the list.
     * @returns A promise for the peer information.
     */
    p2pInfo(peerReference: Commands.PeerReference):
        Promise<Commands.PeerInformation>;

    /**
     * It invites a peer to join a group or will restart a persistent group.
     * 
     * This method uses the underlying command `P2P_INVITE`.
     * 
     * @returns A void promise.
     */
    p2pInvite(): Promise<void>;
}

/**
 * This is an abstract factory that creates the `WpaClient` instance and
 * provides all the necessary methods to create its internal parts.
 */
export declare class WpaClientKit {
    /**
     * It instantiates the concecutive transceiver that will be bound
     * to the `wpa_supplicant`'s interface. This transceiver will be used
     * to send commands to the `wpa_supplicant` daemon
     * and receive replies from it.
     * @returns A transceiver instance.
    */
    makeCommandTransceiver(): Communication.IMessageTransceiver;

    /**
     * It instantiates the eventful transceiver that will be bound
     * to the `wpa_supplicant`'s interface. This transceiver will be used
     * to receive events from the `wpa_supplicant` daemon in the form of
     * data strings. Those are then broken into event name and data
     * to be used as arguments for the transceiver to emit its own events.
     * @returns A transceiver instance.
     */
    makeMonitorTransceiver(): Communication.IEventTransceiver;

    /**
     * It instantiates the parser that will be used by the monitor
     * to break apart the message strings from `wpa_supplicant`,
     * separating the event name and data.
     * @returns A transceiver instance.
     */
    makeEventParser(): Communication.IEventParser;

    /**
     * It instantiates the resolver that will compute the paths
     * to the socket files on the file system. This instance will be used
     * by the `WpaClient` instance directly.
     * @param interfaceName The name of an interface
     * found in the control directory of the `wpa_supplicant` daemon.
     * @returns A path resolver instance.
     */
    makePathResolver(interfaceName: string): PathResolver;

    /**
     * It instantiates the component responsible for transforming
     * high level abstractions into data strings to be transmitted
     * by the transceivers.
     * @returns A message factory instance.
     */
    makeMessageFactory(): Commands.MessageFactory;

    /**
     * It instantiates the class responsible for transforming data strings from
     * the transceivers into high level abstractions.
     * @returns A response parser instance.
     */
    makeResponseParser(): Commands.ResponseParser;

    /**
     * It instantiates the class that maintains a facade to all possible
     * events emitted by the monitor transceiver.
     * @param monitorTransceiver The same transceiver used by the rest of the
     * system, to listen for events.
     */
    makeEventRegistrar(monitorTransceiver: Communication.MonitorTransceiver):
        Commands.EventRegistrar;

    /**
     * It instantiates a new `CommandKit` which the `WpaClient` will use
     * to create the command instances it runs whenever on of its methods are
     * called.
     * @param commandTransceiver The transceiver used to send commands
     * to `wpa_supplicant`.
     * @param monitorTransceiver The transceiver used to monitor
     * the events emitted by the `wpa_supplicant`.
     * @param eventRegistrar The event registrar used to bind handlers
     * to the events emitted by the monitor transceiver.
     * @param messageFactory The factory used by most of the commands
     * to create the data string to be transmitted to the daemon.
     * @param responseParser The response parser used by all commands
     * to create high level abstractions from the data strings comming from
     * the daemon.
     * @returns An instance of `CommandKit`.
     */
    makeCommandKit(
        commandTransceiver: Communication.CommandTransceiver,
        monitorTransceiver: Communication.MonitorTransceiver,
        eventRegistrar: Commands.EventRegistrar,
        messageFactory: Commands.MessageFactory,
        responseParser: Commands.ResponseParser): Commands.CommandKit;

    /**
     * It instantiates a `WpaClient`, the instance through which a developer
     * may have access to all functionalities of this library.
     * @param pathResolver An instance of a `PathResolver`.
     * @param commandKit An instance of a `CommandKit`.
     * @returns An instance of `WpaClient`.
     */
    makeWpaClient(
        pathResolver: PathResolver,
        commandKit: Commands.CommandKit): WpaClient;
}

/**
 * This class is the "out of the box" component necessary to produce
 * a working instance of the `WpaClient`.
 */
export declare class WpaClientFactory {

    /**
     * It instanciates a new `WpaClientFactory` using a default `WpaClientKit`.
     * @param wpaClientKit An instance of `WpaClientKit`.
     */
    constructor(wpaClientKit?: WpaClientKit);

    /**
     * Constructs and returns a new `WpaClient`.
     * @returns An instance of `WpaClient`.
     */
    makeWpaClient(interfaceName: string): WpaClient;
}