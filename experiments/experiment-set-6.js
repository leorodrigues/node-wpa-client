const {
    PathResolver,
    WpaEventParser,
    Communication: { TransceiverKit }
} = require('../index');

const INTERFACE_NAME = process.argv[2] || 'wlan0';

const pathResolver = new PathResolver(INTERFACE_NAME);

const kit = new TransceiverKit();

const commandTransceiver = kit.makeCommandTransceiver();
const monitorTransceiver = kit.makeMonitorTransceiver(new WpaEventParser());

function log(content) {
    console.log(`Monitor: ${JSON.stringify(content)}`);
}

async function run() {

    try {
        await commandTransceiver.connect(pathResolver.getServerSocketPath());
        await commandTransceiver.bind(pathResolver.getControlSocketPath('1'));

        await monitorTransceiver.connect(pathResolver.getServerSocketPath());
        await monitorTransceiver.bind(pathResolver.getControlSocketPath('2'));

    } catch(error) {
        console.log(error);
        return;
    }

    try {
        console.log('\nRunning ping '.padEnd(80, '.'));
        await commandTransceiver.transmit('PING');
        console.log(`Ping reply: ${await commandTransceiver.receive()}`);

        monitorTransceiver.on('UNKNOWN-MESSAGE', m => log(m));
        monitorTransceiver.on('COMMAND-CONFIRMED', () => log('confirmed'));
        monitorTransceiver.on('CTRL-EVENT-SCAN-STARTED', () => log('started'));
        monitorTransceiver.on('CTRL-EVENT-BSS-ADDED', data => log(data));
        monitorTransceiver.on('CTRL-EVENT-BSS-REMOVED', data => log(data));

        console.log('\nRunning attach '.padEnd(80, '.'));
        await monitorTransceiver.transmit('ATTACH');

        console.log('\nRunning scan '.padEnd(80, '.'));
        await commandTransceiver.transmit('SCAN 5');
        console.log(`Scan reply: ${await commandTransceiver.receive()}`);

        await new Promise((resolve) => setTimeout(resolve, 6000));

        console.log('\nRunning detach '.padEnd(80, '.'));
        await monitorTransceiver.transmit('DETACH');
    } finally {
        await commandTransceiver.close();
        await monitorTransceiver.close();
    }
}

setTimeout(run, 5000);