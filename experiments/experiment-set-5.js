
const { WpaClientFactory } = require('../index');

const factory = new WpaClientFactory();

const interfaceName = process.argv[2] || 'wlan0';

const client = factory.makeWpaClient(interfaceName);

async function run() {
    try {
        await client.open();
    } catch(error) {
        console.log(error);
        return;
    }

    try {
        console.log('\nRunning reconnect '.padEnd(80, '.'));
        let result = await client.reconnect();
        console.log(JSON.stringify(result, 'utf-8', 4));

        console.log('\nRunning reassociate '.padEnd(80, '.'));
        result = await client.reassociate();
        console.log(JSON.stringify(result, 'utf-8', 4));

        console.log('\ndone');
    } finally {
        await client.close();
    }
}

setTimeout(run, 5000);

// run();