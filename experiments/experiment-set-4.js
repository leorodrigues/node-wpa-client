
const { WpaClientFactory } = require('../index');

const factory = new WpaClientFactory();

const interfaceName = process.argv[2] || 'wlan0';

const client = factory.makeWpaClient(interfaceName);

async function run() {
    try {
        await client.open();
    } catch(error) {
        console.log(error);
        return;
    }

    try {
        console.log('\nRunning status '.padEnd(80, '.'));
        const status = await client.getStatus();
        console.log(JSON.stringify(status, 'utf-8', 4));

        console.log('\ndone');
    } finally {
        await client.close();
    }
}

run();