
const { WpaClientFactory, Commands: { Period } } = require('../index');

const factory = new WpaClientFactory();

const interfaceName = process.argv[2] || 'wlan0';

const client = factory.makeWpaClient(interfaceName);

async function run() {
    try {
        await client.open();
    } catch(error) {
        console.log(error);
        return;
    }

    try {
        const fiveSeconds = new Period(5);
        console.log('\nRunning scan '.padEnd(80, '.'));
        console.log(JSON.stringify(await client.scan(fiveSeconds), 'utf-8', 4));

        console.log('\nRunning get scan results '.padEnd(80, '.'));
        let networks = await client.getScanResults();
        console.log(JSON.stringify(networks, 'utf-8', 4));

        console.log('\nRunning list networks '.padEnd(80, '.'));
        networks = await client.listNetworkEntries();
        console.log(JSON.stringify(networks, 'utf-8', 4));

        console.log('\ndone');
    } finally {
        await client.close();
    }
}

run();