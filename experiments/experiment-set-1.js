
const { WpaClientFactory } = require('../index');

const factory = new WpaClientFactory();

const interfaceName = process.argv[2] || 'wlan0';

const client = factory.makeWpaClient(interfaceName);

async function run() {
    try {
        await client.open();
    } catch(error) {
        console.log(error);
        return;
    }

    try {
        console.log('\nRunning ping '.padEnd(80, '.'));
        console.log(JSON.stringify(await client.ping()));
        
        console.log('\nRunning list interfaces '.padEnd(80, '.'));
        console.log(JSON.stringify(await client.listInterfaces()));

        console.log('\ndone');
    } finally {
        await client.close();
    }
}

run();