
const { WpaClientFactory } = require('../index');

const factory = new WpaClientFactory();

const interfaceName = process.argv[2] || 'wlan0';

const client = factory.makeWpaClient(interfaceName);

async function run() {
    try {
        await client.open();
    } catch(error) {
        console.log(error);
        return;
    }

    try {
        console.log('\nRunning add network '.padEnd(80, '.'));
        const reference = await client.addNetworkEntry();
        console.log(JSON.stringify(reference, 'utf-8', 4));

        console.log('\nRunning list networks '.padEnd(80, '.'));
        let networks = await client.listNetworkEntries();
        console.log(JSON.stringify(networks, 'utf-8', 4));

        const info = reference.makeNetworkInfo({ ssid: 'My network' });

        await client.patchNetworkEntry(info);

        console.log('\nRunning list networks '.padEnd(80, '.'));
        networks = await client.listNetworkEntries();
        console.log(JSON.stringify(networks, 'utf-8', 4));

        console.log('\ndone');
    } finally {
        await client.close();
    }
}

run();