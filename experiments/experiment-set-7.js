
const { WpaClientFactory, Commands: { Period } } = require('../index');

const factory = new WpaClientFactory();

const interfaceName = process.argv[2] || 'wlan0';

const client = factory.makeWpaClient(interfaceName);

function run() {
    return new Promise((resolve) => {
        client.open().then(() => {
            client.standByForEvents(registrar => {
                registrar.onScanStarted(() => console.log('scanning...'));
                registrar.onP2PFindStopped(() => {
                    console.log('Stopped finding');
                    resolve();
                });
                registrar.onP2PDeviceFound(d => console.log(d));
            });
    
            console.log('\nRunning ping '.padEnd(80, '.'));

            return client.ping();
        }).then(pingReply => {
            console.log(JSON.stringify(pingReply));

            console.log('\nRunning p2p_find 5'.padEnd(80, '.'));
            return client.p2pFind(new Period(5));
        });
    }).then(() => {
        console.log('done');
        client.close();
    });
}

run();