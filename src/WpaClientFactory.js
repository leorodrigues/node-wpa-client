
const { WpaClientKit } = require('./WpaClientKit');

const DEFAULT_WPA_CLIENT_KIT = new WpaClientKit();

class WpaClientFactory {

    /* istanbul ignore next */
    constructor(wpaClientKit = DEFAULT_WPA_CLIENT_KIT) {
        this.wpaClientKit = wpaClientKit;
    }

    makeWpaClient(interfaceName, controlId) {
        const kit = this.wpaClientKit;
        const eventParser = kit.makeEventParser();
        const monitorTransceiver = kit.makeMonitorTransceiver(eventParser);
        return this.wpaClientKit.makeWpaClient(
            kit.makePathResolver(interfaceName, controlId),
            kit.makeCommandKit(
                kit.makeCommandTransceiver(),
                monitorTransceiver,
                kit.makeEventRegistrar(monitorTransceiver),
                kit.makeResponseParser()));
    }
}

module.exports = { WpaClientFactory };