
const fs = require('fs');

class PathResolver {
    constructor(interfaceName, controlId) {
        this.currentId = controlId || (new Date()).getTime().toString(16);
        this.interfaceName = interfaceName;
        this.serverSocketPath = `/var/run/wpa_supplicant/${interfaceName}`;
        this.baseControlSocketPath = `/tmp/wpa_ctrl_${this.currentId}`;
    }

    getServerSocketPath() {
        fs.accessSync(this.serverSocketPath);
        return this.serverSocketPath;
    }

    getControlSocketPath(localId = '0') {
        return `${this.baseControlSocketPath}-${localId}.sock`;
    }
}

module.exports = { PathResolver };