
const { MessageCommand } = require('./MessageCommand');

class Reassociate extends MessageCommand {
    constructor(commandTransceiver, responseParser) {
        super(commandTransceiver);
        this.responseParser = responseParser;
    }

    async invoke() {
        await this.transmit('REASSOCIATE');
        const reply = await this.receive();
        return this.responseParser.parseOk(reply) === 'OK';
    }
}

module.exports = { Reassociate };