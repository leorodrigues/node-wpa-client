
const { MessageCommand } = require('./MessageCommand');

class GetStatus extends MessageCommand {
    constructor(commandTransceiver, responseParser) {
        super(commandTransceiver);
        this.responseParser = responseParser;
    }

    async invoke() {
        await this.transmit('STATUS');
        const reply = await this.receive();
        return this.responseParser.parseStatus(reply);
    }
}

module.exports = { GetStatus };