
const { MessageCommand } = require('./MessageCommand');

class SelectNetworkEntry extends MessageCommand {
    constructor(reference, commandTransceiver, responseParser) {
        super(commandTransceiver);
        this.responseParser = responseParser;
        this.reference = reference;
    }

    async invoke() {
        await this.transmit(this.reference.selectNetworkCmdString);
        const reply = await this.receive();
        return this.responseParser.parseOk(reply) === 'OK';
    }
}

module.exports = { SelectNetworkEntry };