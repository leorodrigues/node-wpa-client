
class PayloadParameterArray extends Array {
    constructor(...items) {
        super();
        this.push(...items);
    }

    appendToPayload(payloadString) {
        let result = payloadString;
        for (const p of this)
            result = p.appendToPayload(result);
        return result;
    }
}

module.exports = { PayloadParameterArray };