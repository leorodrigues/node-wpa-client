
const { MessageCommand } = require('./MessageCommand');

class PatchNetworkEntry extends MessageCommand {
    constructor(networkInfo, commandTransceiver, responseParser) {
        super(commandTransceiver);
        this.networkInfo = networkInfo;
        this.responseParser = responseParser;
    }

    async invoke() {
        for (const payload of this.networkInfo.iterateSetPayloads()) {
            await this.transmit(payload);
            let reply = await this.receive();
            reply = this.responseParser.parseOk(reply);
            this.confirmReply(reply, 'OK');
        }

        return this.networkInfo;
    }
}

module.exports = { PatchNetworkEntry };