
const { MessageCommand } = require('./MessageCommand');

class DisableNetworkEntry extends MessageCommand {
    constructor(networkEntryReference, commandTransceiver, responseParser) {
        super(commandTransceiver);
        this.responseParser = responseParser;
        this.networkEntryReference = networkEntryReference;
    }

    async invoke() {
        const reference = this.networkEntryReference;
        await this.transmit(reference.disableNetworkCmdString);
        const reply = await this.receive();
        return this.responseParser.parseOk(reply) === 'OK';
    }
}

module.exports = { DisableNetworkEntry };