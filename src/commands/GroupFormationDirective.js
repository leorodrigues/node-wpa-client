
class GroupFormationDirective {
    constructor(action = 'none', persistent = false) {
        this.persistent = persistent;
        this.action = action;
    }

    appendToPayload(payloadString) {
        return this.action === 'none'
            ? payloadString
            : this.persistent
                ? `${payloadString} persistent ${this.action}`
                : `${payloadString} ${this.action}`;
    }

    static get NONE() {
        return DIRECTIVE_NONE;
    }

    static get JOIN() {
        return DIRECTIVE_JOIN;
    }

    static get AUTH() {
        return DIRECTIVE_AUTH;
    }

    static get JOIN_AND_PERSIST() {
        return DIRECTIVE_JOIN_AND_PERSIST;
    }

    static get AUTH_AND_PERSIST() {
        return DIRECTIVE_AUTH_AND_PERSIST;
    }
}

const DIRECTIVE_NONE = new GroupFormationDirective();

const DIRECTIVE_JOIN = new GroupFormationDirective('join');

const DIRECTIVE_AUTH = new GroupFormationDirective('auth');

const DIRECTIVE_JOIN_AND_PERSIST = new GroupFormationDirective('join', true);

const DIRECTIVE_AUTH_AND_PERSIST = new GroupFormationDirective('auth', true);

module.exports = { GroupFormationDirective };