
/* istanbul ignore next */
class EventRegistrar {
    constructor(monitorTransceiver) {
        this.monitorTransceiver = monitorTransceiver;
    }

    onP2PDeviceFound(handle) {
        this.monitorTransceiver.on('P2P-DEVICE-FOUND', handle);
    }

    onP2PDeviceLost(handle) {
        this.monitorTransceiver.on('P2P-DEVICE-LOST', handle);
    }

    onP2PFindStopped(handle) {
        this.monitorTransceiver.on('P2P-FIND-STOPPED', handle);
    }

    onScanStarted(handle) {
        this.monitorTransceiver.on('CTRL-EVENT-SCAN-STARTED', handle);
    }

    standByForEvents() {
        this.monitorTransceiver.standByForEvents();
    }
}

module.exports = { EventRegistrar };