

class IncorrectProtocolError extends Error {
    constructor(payload) {
        super('Incorrect protocol.');
        this.payload = payload;
    }
}

module.exports = { IncorrectProtocolError };