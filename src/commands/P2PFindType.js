
class P2PFindType {
    constructor(value) {
        this.value = value || 'none';
    }

    appendToPayload(payloadString) {
        return this.value !== 'none'
            ? `${payloadString} type=${this.value}`
            : payloadString;
    }

    static get NONE() {
        return P2P_FIND_TYPE_NONE;
    }

    static get SOCIAL() {
        return P2P_FIND_TYPE_SOCIAL;
    }

    static get PROGRESSIVE() {
        return P2P_FIND_TYPE_PROGRESSIVE;
    }
}

const P2P_FIND_TYPE_NONE = new P2PFindType();

const P2P_FIND_TYPE_SOCIAL = new P2PFindType('social');

const P2P_FIND_TYPE_PROGRESSIVE = new P2PFindType('progressive');

module.exports = { P2PFindType };