
class ScanResultEntry {
    constructor(bssid, frequency, signalLevel, flags, ssid) {
        this.bssid = bssid;
        this.frequency = frequency;
        this.signalLevel = signalLevel;
        this.flags = flags;
        this.ssid = ssid;
    }
}

module.exports = { ScanResultEntry };