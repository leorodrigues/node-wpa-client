
class P2PConnectionParams {
    constructor(peerAddress, provisioningMethod, groupFormationDirective) {
        this.peerAddress = peerAddress;
        this.provisioningMethod = provisioningMethod;
        this.groupFormationDirective = groupFormationDirective;
    }

    appendToPayload(payloadString) {
        return this.groupFormationDirective.appendToPayload(
            this.provisioningMethod.appendToPayload(
                this.peerAddress.appendToPayload(payloadString)));
    }
}

module.exports = { P2PConnectionParams };