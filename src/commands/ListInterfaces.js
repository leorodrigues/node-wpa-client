
const { MessageCommand } = require('./MessageCommand');

class ListInterfaces extends MessageCommand {
    constructor(commandTransceiver, responseParser) {
        super(commandTransceiver);
        this.responseParser = responseParser;
    }

    async invoke() {
        await this.transmit('INTERFACES');
        return this.responseParser.parseListInterfaces(await this.receive());
    }
}

module.exports = { ListInterfaces };