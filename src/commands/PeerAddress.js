
class PeerAddress {
    constructor(addressString) {
        this.addressString = addressString || null;
    }

    appendToPayload(payloadString) {
        if (this.address === null) return payloadString;
        return `${payloadString} ${this.addressString}`;
    }

    static get NONE() {
        return ADDRESS_NONE;
    }
}

const ADDRESS_NONE = new PeerAddress();

module.exports = { PeerAddress };