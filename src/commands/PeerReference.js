/**
 * A peer reference to one of the elements on the list created by
 * the `WpaClient.p2pFind` operation.
 * @see WpaClient#p2pFind
 */
class PeerReference {

    /**
     * Creates a new instance of `PeerReference`.
     * @see PeerReference
     * @param {string} id An id for one of the elements in the current list.
     */
    constructor(id) {
        this.id = id;
    }

    toString() {
        return `${this.id}`;
    }

    /**
     * This value references the first element of the list created by
     * the `WpaClinet.p2pFind` operation. Is intended to be used in conjunction
     * with the `WpaClient.p2pInfo` method to retrieve the element.
     * @see WpaClient#p2pFind
     * @see WpaClient#p2pInfo
     */
    static get FIRST() {
        return FIRST;
    }

    /**
     * This value references the next element of the list created by
     * the `WpaClient.p2pFind` operation. Is intended to be used in conjunction
     * with the `WpaClient.p2pInfo` method to perform an iteration on the list.
     * @see WpaClient#p2pFind
     * @see WpaClient#p2pInfo
     */
    static get NEXT() {
        return NEXT;
    }
}

const FIRST = new PeerReference('FIRST');

const NEXT = new PeerReference('NEXT');

module.exports = { PeerReference };