
const { SocketLifeCycleCommand } = require('./SocketLifeCycleCommand');

class OpenTransceiver extends SocketLifeCycleCommand {
    constructor(socketLifeCycle, commandSockPath, controlSocketPath) {
        super(socketLifeCycle);
        this.commandSockPath = commandSockPath;
        this.controlSocketPath = controlSocketPath;
    }

    async invoke() {
        await this.socketLifeCycle.connect(this.commandSockPath);
        await this.socketLifeCycle.bind(this.controlSocketPath);
    }
}

module.exports = { OpenTransceiver };