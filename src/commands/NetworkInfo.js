
const { GetNetworkExchange } = require('./GetNetworkExchange');

const FIELDS = [
    'reference', 'ssid', 'psk', 'keyManagement', 'identity', 'password'
];

const isNotBlank = o => o !== undefined && o !== null;

class NetworkInfo {
    constructor(reference, ssid, psk, keyManagement, identity, password) {
        this.reference = reference;
        this.ssid = ssid;
        this.psk = psk;
        this.keyManagement = keyManagement;
        this.identity = identity;
        this.password = password;
    }

    equals(another) {
        return another === this
            || this.isTheSameEntityAs(another)
            && this.hasTheSameContentsOf(another);
    }

    hasTheSameContentsOf(another) {
        return this.ssid === another.ssid
            && this.psk === another.psk
            && this.keyManagement === another.keyManagement
            && this.identity === another.identity
            && this.password === another.password;
    }

    isTheSameEntityAs(another) {
        return another instanceof NetworkInfo
            && isNotBlank(this.reference)
            && isNotBlank(another.reference)
            && this.reference.equals(another.reference);
    }

    *iterateSetPayloads() {
        const reference = this.reference;

        if (this.ssid)
            yield reference.interpolate(
                `SET_NETWORK :id ssid "${this.ssid}"`);

        if (this.psk)
            yield reference.interpolate(
                `SET_NETWORK :id psk "${this.psk}"`);

        if (this.keyManagement)
            yield reference.interpolate(
                `SET_NETWORK :id key_mgmt "${this.keyManagement}"`);

        if (this.identity)
            yield reference.interpolate(
                `SET_NETWORK :id identity "${this.identity}"`);

        if (this.password)
            yield reference.interpolate(
                `SET_NETWORK :id password "${this.password}"`);
    }

    static *iterateGetNetworkExchanges(id) {
        yield new GetNetworkExchange(id, 'ssid');
        yield new GetNetworkExchange(id, 'psk');
        yield new GetNetworkExchange(id, 'key_mgmt', 'keyManagement');
        yield new GetNetworkExchange(id, 'identity');
        yield new GetNetworkExchange(id, 'password');
    }

    static makeInstance(attributes) {
        return new NetworkInfo(...FIELDS.map(f => attributes[f]));
    }
}

module.exports = { NetworkInfo };