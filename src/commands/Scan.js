
const { Period } = require('./Period');
const { MessageCommand } = require('./MessageCommand');

class Scan extends MessageCommand {
    constructor(period, commandTransceiver, responseParser) {
        super(commandTransceiver);
        this.period = period || Period.UNKNOWN;
        this.responseParser = responseParser;
    }

    async invoke() {
        const cmdString = this.period.appendToPayload('SCAN');

        await this.transmit(cmdString);
        const reply = this.responseParser.parseOk(await this.receive());
        this.confirmReply(reply, 'OK');
        return reply;
    }
}

module.exports = { Scan };