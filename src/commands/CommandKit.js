
const { DisableNetworkEntry } = require('./DisableNetworkEntry');
const { SelectNetworkEntry } = require('./SelectNetworkEntry');
const { EnableNetworkEntry } = require('./EnableNetworkEntry');
const { ListNetworkEntries } = require('./ListNetworkEntries');
const { RemoveNetworkEntry } = require('./RemoveNetworkEntry');
const { PatchNetworkEntry } = require('./PatchNetworkEntry');
const { StandByForEvents } = require('./StandByForEvents');
const { CloseTransceiver } = require('./CloseTransceiver');
const { AddNetworkEntry } = require('./AddNetworkEntry');
const { OpenTransceiver } = require('./OpenTransceiver');
const { ListInterfaces } = require('./ListInterfaces');
const { GetScanResults } = require('./GetScanResults');
const { GetNetworkInfo } = require('./GetNetworkInfo');
const { Reassociate } = require('./Reassociate');
const { Disconnect } = require('./Disconnect');
const { GetStatus } = require('./GetStatus');
const { Reconnect } = require('./Reconnect');
const { Ping } = require('./Ping');
const { Scan } = require('./Scan');

const { EventCommand } = require('./EventCommand');
const { PayloadParameterArray } = require('./PayloadParameterArray');


/* istanbul ignore next */
class CommandKit {
    constructor(
        messageTransceiver,
        eventTransceiver,
        eventRegistrar,
        responseParser) {
        this.messageTransceiver = messageTransceiver;
        this.eventTransceiver = eventTransceiver;
        this.eventRegistrar = eventRegistrar;
        this.responseParser = responseParser;
    }

    makeOpenCommandTransceiver(wpaSocketPath, controlSocketPath) {
        return new OpenTransceiver(
            this.messageTransceiver, wpaSocketPath, controlSocketPath);
    }

    makeOpenMonitorTransceiver(wpaSocketPath, controlSocketPath) {
        return new OpenTransceiver(
            this.eventTransceiver, wpaSocketPath, controlSocketPath);
    }

    makeStandByForEvents(wpaClient, configure) {
        return new StandByForEvents(this.eventRegistrar, wpaClient, configure);
    }

    makeCloseMonitorTransceiver() {
        return new CloseTransceiver(this.messageTransceiver);
    }

    makeCloseCommandTransceiver() {
        return new CloseTransceiver(this.eventTransceiver);
    }

    makePing() {
        return new Ping(this.messageTransceiver, this.responseParser);
    }

    makeAttach() {
        return new EventCommand(this.eventTransceiver, 'ATTACH');
    }

    makeDetach() {
        return new EventCommand(this.eventTransceiver, 'DETACH');
    }

    makeListInterfaces() {
        return new ListInterfaces(this.messageTransceiver, this.responseParser);
    }

    makeScan(period) {
        return new Scan(period, this.messageTransceiver, this.responseParser);
    }

    makeListNetworkEntries() {
        return new ListNetworkEntries(
            this.messageTransceiver, this.responseParser);
    }

    makeAddNetworkEntry() {
        return new AddNetworkEntry(
            this.messageTransceiver, this.responseParser);
    }

    makePatchNetworkEntry(networkInfo) {
        return new PatchNetworkEntry(
            networkInfo, this.messageTransceiver, this.responseParser);
    }

    makeSelectNetworkEntry(networkEntryReference) {
        return new SelectNetworkEntry(
            networkEntryReference,
            this.messageTransceiver,
            this.responseParser);
    }

    makeEnableNetworkEntry(networkEntryReference) {
        return new EnableNetworkEntry(
            networkEntryReference,
            this.messageTransceiver,
            this.responseParser);
    }

    makeDisableNetworkEntry(networkEntryReference) {
        return new DisableNetworkEntry(
            networkEntryReference,
            this.messageTransceiver,
            this.responseParser);
    }

    makeRemoveNetworkEntry(networkEntryReference) {
        return new RemoveNetworkEntry(
            networkEntryReference,
            this.messageTransceiver,
            this.responseParser);
    }

    makeGetNetworkInfo(networkEntryReference) {
        return new GetNetworkInfo(
            networkEntryReference,
            this.messageTransceiver,
            this.responseParser);
    }

    makeDisconnect() {
        return new Disconnect(this.messageTransceiver, this.responseParser);
    }

    makeReassociate() {
        return new Reassociate(this.messageTransceiver, this.responseParser);
    }

    makeReconnect() {
        return new Reconnect(this.messageTransceiver, this.responseParser);
    }

    makeGetScanResults() {
        return new GetScanResults(this.messageTransceiver, this.responseParser);
    }

    makeGetStatus() {
        return new GetStatus(this.messageTransceiver, this.responseParser);
    }

    makeP2PFind(period, type) {
        const transceiver = this.eventTransceiver;
        const parameters = new PayloadParameterArray(period, type);
        return new EventCommand(transceiver, 'P2P_FIND', parameters);
    }

    makeP2PStopFind() {
        return new EventCommand(this.eventTransceiver, 'P2P_FIND_STOP');
    }

    makeP2PConnect(p2pConnectionParams) {
        const transceiver = this.eventTransceiver;
        const parameters = new PayloadParameterArray(p2pConnectionParams);
        return new EventCommand(transceiver, 'P2P_CONNECT', parameters);
    }

    makeP2PAddGroup() {
        return new EventCommand(this.eventTransceiver, 'P2P_GROUP_ADD');
    }

    makeP2PRemoveGroup() {
        return new EventCommand(this.eventTransceiver, 'P2P_GROUP_REMOVE');
    }

    makeP2PRequestProvisionDiscovery(peerAddress, peerPinNegotiationMethod) {
        const transceiver = this.eventTransceiver;
        const parameters = new PayloadParameterArray(
            peerAddress, peerPinNegotiationMethod);
        return new EventCommand(transceiver, 'P2P_PROV_DISC', parameters);
    }

    makeP2PListen(period) {
        const transceiver = this.eventTransceiver;
        const parameters = new PayloadParameterArray(period);
        return new EventCommand(transceiver, 'P2P_LISTEN', parameters);
    }

    makeP2PGetPassphrase() {
        return new EventCommand(this.eventTransceiver, 'P2P_GET_PASSPHRASE');
    }
}

module.exports = { CommandKit };