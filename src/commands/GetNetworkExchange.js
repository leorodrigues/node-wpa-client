
const firstLine = text => text.split('\n')[0];

class GetNetworkExchange {
    constructor(id, property, alias) {
        this.id = id;
        this.property = property;
        this.alias = alias;
    }

    renderRequest() {
        return `GET_NETWORK ${this.id} ${this.property}`;
    }

    parseResponse(rawData) {
        const result = firstLine(rawData);
        if (result !== 'FAIL')
            return result;
    }

    collect(value, attributes) {
        if (value)
            attributes[this.alias || this.property] = value;
    }
}

module.exports = { GetNetworkExchange };