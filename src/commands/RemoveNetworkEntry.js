const { MessageCommand } = require('./MessageCommand');

class RemoveNetworkEntry extends MessageCommand {
    constructor(reference, commandTransceiver, responseParser) {
        super(commandTransceiver);
        this.responseParser = responseParser;
        this.reference = reference;
    }

    async invoke() {
        await this.transmit(this.reference.removeNetworkCmdString);
        const reply = await this.receive();
        return this.responseParser.parseOk(reply) === 'OK';
    }
}

module.exports = { RemoveNetworkEntry };