
const firstLine = l => l.split('\n').shift();

class WpaEventParser {
    parseName(message) {
        message = firstLine(message);
        let pattern = /^<\d+>((AP|CTRL|P2P|WPS)[\w-]+).*$/;
        if (pattern.test(message)) {
            const [,name] = message.match(pattern);
            return name;
        }
        pattern = /^OK$/g;
        if (pattern.test(message))
            return 'COMMAND-CONFIRMED';
        return 'UNKNOWN-MESSAGE';
    }

    parseData(message) {
        message = firstLine(message);
        let pattern = /^<(\d+)>((AP|CTRL|P2P|WPS)[\w-]+)\s*(.*)$/;
        if (pattern.test(message)) {
            const [,level,name,,data] = message.match(pattern);
            return { level, name, data };
        }
        pattern = /^OK$/g;
        if (pattern.test(message))
            return { };
        return message;
    }
}

module.exports = { WpaEventParser };