
const { MessageCommand } = require('./MessageCommand');

class Disconnect extends MessageCommand {
    constructor(commandTransceiver, responseParser) {
        super(commandTransceiver);
        this.responseParser = responseParser;
    }

    async invoke() {
        await this.transmit('DISCONNECT');
        const reply = await this.receive();
        return this.responseParser.parseOk(reply) === 'OK';
    }
}

module.exports = { Disconnect };