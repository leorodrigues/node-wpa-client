
class PeerPinNegotiationMethod {
    constructor(value = 'none') {
        this.value = value;
    }

    appendToPayload(payloadString) {
        return this.value !== 'none'
            ? `${payloadString} ${this.value}`
            : payloadString;
    }

    static get NONE() {
        return NONE;
    }

    static get LABEL() {
        return LABEL;
    }

    static get DISPLAY() {
        return DISPLAY;
    }

    static get KEYPAD() {
        return KEYPAD;
    }
}

const NONE = new PeerPinNegotiationMethod();

const LABEL = new PeerPinNegotiationMethod('label');

const DISPLAY = new PeerPinNegotiationMethod('display');

const KEYPAD = new PeerPinNegotiationMethod('keypad');

module.exports = { PeerPinNegotiationMethod };