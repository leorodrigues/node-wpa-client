const { IncorrectProtocolError } = require('./IncorrectProtocolError');

class MessageCommand {
    constructor(commandTransceiver) {
        this.commandTransceiver = commandTransceiver;
    }

    transmit(message) {
        return this.commandTransceiver.transmit(message);
    }

    receive() {
        return this.commandTransceiver.receive();
    }

    confirmReply(actualReply, expectedReply) {
        if (actualReply !== expectedReply)
            throw new IncorrectProtocolError(actualReply);
        return actualReply;
    }
}

module.exports = { MessageCommand };