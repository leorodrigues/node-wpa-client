
/**
 * This is a DTO class carrying the parameters used by `WpaClient.p2pConnect`.
 */
class PeerConnectionParams {
    /**
     * Creates a new instance of `PeerConnectionParams`.
     * 
     * @param {string} address Address of the peer device
     * @param {string} authType Authentication method
     * @param {string} pin A set of numbers to be exchanged between
     * client and server
     * @param {string} ownerParams Various definitions
     */
    constructor(address, authType, pin, ownerParams) {
        this.ownerParams = ownerParams;
        this.authType = authType;
        this.address = address;
        this.pin = pin;
    }
}

module.exports = { PeerConnectionParams };