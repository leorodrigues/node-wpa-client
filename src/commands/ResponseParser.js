
const { Status } = require('./Status');
const { ScanResultEntry } = require('./ScanResultEntry');
const { NetworkEntry } = require('./NetworkEntry');
const { NetworkEntryReference } = require('./NetworkEntryReference');

const excludingEmptyString = s => s !== '';

const firstLineOnly = rawData => rawData.split('\n').shift();

const allLines = rawData => rawData.split('\n').filter(excludingEmptyString);

const allFields = rawData => rawData.split('\t');

const flagList = flags => [...flags.matchAll(/\[([\w+.-]+)\]/g)].map(m => m[1]);

const toKeyValuePairs = s => s.split('=');

class ResponseParser {
    parseListInterfaces(rawData) {
        return allLines(rawData);
    }

    parseOk(rawData) {
        return firstLineOnly(rawData);
    }

    parsePong(rawData) {
        return firstLineOnly(rawData);
    }

    parseInt(rawData) {
        return parseInt(firstLineOnly(rawData));
    }

    parseScanResult(rawData) {
        const { lineToScanResultEntry } = ResponseParser;
        return allLines(rawData).splice(1).map(lineToScanResultEntry);
    }

    parseNetworkEntries(rawData) {
        const { lineToNetworkEntry } = ResponseParser;
        return allLines(rawData).splice(1).map(lineToNetworkEntry);
    }

    parseStatus(rawData) {
        const set = (p, c) => (p[c[0]] = c[1]) && p;
        const map = allLines(rawData).map(toKeyValuePairs).reduce(set, { });
        return new Status(
            map.id,
            map.uuid,
            map.bssid,
            map.ssid,
            parseInt(map.freq),
            map.mode,
            map.parwise_cipher,
            map.group_cipher,
            map.key_mgmt,
            map.wpa_state,
            map.address,
            map.ip_address,
            map.p2p_device_address);
    }

    static lineToNetworkEntry(line) {
        const [ id, ssid, bssid, flags = [ ] ] = allFields(line);
        console.log([id, ssid, bssid, flags]);
        const reference = new NetworkEntryReference(id);
        return new NetworkEntry(reference, ssid, bssid, flagList(flags));
    }

    static lineToScanResultEntry(line) {
        const [ bssid, frequency, signalLevel, flags, ssid ] = allFields(line);
        return new ScanResultEntry(
            bssid,
            parseInt(frequency),
            parseInt(signalLevel),
            flagList(flags),
            ssid);
    }
}

module.exports = { ResponseParser };