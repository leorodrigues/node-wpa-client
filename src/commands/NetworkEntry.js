
const FIELDS = [ 'reference', 'ssid', 'bssid', 'flags' ];

const isNotBlank = o => o !== undefined && o !== null;

class NetworkEntry {
    constructor(reference, ssid, bssid, flags) {
        this.reference = reference;
        this.ssid = ssid;
        this.bssid = bssid;
        this.flags = flags;
    }

    equals(another) {
        return another === this
            || this.isTheSameEntityAs(another)
            && this.hasTheSameContentsOf(another);
    }

    hasTheSameContentsOf(another) {
        return this.ssid === another.ssid
            && this.bssid === another.bssid
            && this.flags === another.flags;
    }

    isTheSameEntityAs(another) {
        return another instanceof NetworkEntry
            && isNotBlank(this.reference)
            && isNotBlank(another.reference)
            && this.reference.equals(another.reference);
    }

    static makeInstance(attributes) {
        return new NetworkEntry(...FIELDS.map(f => attributes[f]));
    }
}

module.exports = { NetworkEntry };