
const { MessageCommand } = require('./MessageCommand');

class ListNetworkEntries extends MessageCommand {
    constructor(commandTransceiver, responseParser) {
        super(commandTransceiver);
        this.responseParser =responseParser;
    }

    async invoke() {
        await this.transmit('LIST_NETWORKS');
        const reply = await this.receive();
        return this.responseParser.parseNetworkEntries(reply);
    }
}

module.exports = { ListNetworkEntries };