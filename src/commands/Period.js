class Period {

    constructor(value) {
        this.value = value;
    }

    holdOn() {
        const timeout = this.value * 1000;
        return this !== Period.UNKNOWN
            ? new Promise(resolve => setTimeout(resolve, timeout))
            : Promise.resolve();
    }

    appendToPayload(payloadString) {
        return this !== UNKNOWN
            ? `${payloadString} ${this.value}`
            : payloadString;
    }

    static get UNKNOWN() {
        return UNKNOWN;
    }
}

const UNKNOWN = new Period(-1);

module.exports = { Period };