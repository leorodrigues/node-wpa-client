
class StandByForEvents {
    constructor(eventRegistrar, wpaClient, configureRegistrar) {
        this.configureRegistrar = configureRegistrar;
        this.eventRegistrar = eventRegistrar;
        this.wpaClient = wpaClient;
    }

    async invoke() {
        this.configureRegistrar(this.eventRegistrar, this.wpaClient);
        this.eventRegistrar.standByForEvents();
    }
}

module.exports = { StandByForEvents };