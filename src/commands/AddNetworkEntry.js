
const { MessageCommand } = require('./MessageCommand');
const { NetworkEntryReference } = require('./NetworkEntryReference');

class AddNetworkEntry extends MessageCommand {
    constructor(commandTransceiver, responseParser) {
        super(commandTransceiver);
        this.responseParser = responseParser;
    }

    async invoke() {
        await this.transmit('ADD_NETWORK');
        const id = this.responseParser.parseInt(await this.receive());
        return new NetworkEntryReference(id);
    }
}

module.exports = { AddNetworkEntry };