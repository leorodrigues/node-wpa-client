
/* istanbul ignore next */
class Status {
    constructor(
        id,
        uuid,
        bssid,
        ssid,
        frequency,
        mode,
        parwiseCipher,
        groupCipher,
        keyManagement,
        wpaState,
        address,
        ipAddress,
        p2pDeviceAddress) {

        this.id = id;
        this.uuid = uuid;
        this.bssid = bssid;
        this.ssid = ssid;
        this.frequency = frequency;
        this.mode = mode;
        this.parwiseCipher = parwiseCipher;
        this.groupCipher = groupCipher;
        this.keyManagement = keyManagement;
        this.wpaState = wpaState;
        this.address = address;
        this.ipAddress = ipAddress;
        this.p2pDeviceAddress = p2pDeviceAddress;
    }
}

module.exports = { Status };