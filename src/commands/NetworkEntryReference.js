const { NetworkEntry } = require('./NetworkEntry');
const { NetworkInfo } = require('./NetworkInfo');

class NetworkEntryReference {
    constructor(id) {
        this.id = id;
    }

    get enableNetworkCmdString() {
        return `ENABLE_NETWORK ${this.id}`;
    }

    get disableNetworkCmdString() {
        return `DISABLE_NETWORK ${this.id}`;
    }

    get selectNetworkCmdString() {
        return `SELECT_NETWORK ${this.id}`;
    }

    get removeNetworkCmdString() {
        return `REMOVE_NETWORK ${this.id}`;
    }

    equals(another) {
        return this.id === another.id;
    }

    interpolate(text) {
        return text.replace(/:id/g, this.id);
    }

    /* istanbul ignore next */
    *iterateGetNetworkExchanges() {
        yield *NetworkInfo.iterateGetNetworkExchanges(this.id);
    }

    /* istanbul ignore next */
    makeNetworkEntry(attributes) {
        return NetworkEntry.makeInstance({ reference: this, ...attributes });
    }

    /* istanbul ignore next */
    makeNetworkInfo(attributes) {
        return NetworkInfo.makeInstance({ reference: this, ...attributes });
    }
}

module.exports = { NetworkEntryReference };