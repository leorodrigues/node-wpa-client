
const { SocketLifeCycleCommand } = require('./SocketLifeCycleCommand');

class CloseTransceiver extends SocketLifeCycleCommand {
    async invoke() {
        return this.socketLifeCycle.close();
    }
}

module.exports = { CloseTransceiver };