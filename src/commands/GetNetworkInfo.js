
const { MessageCommand } = require('./MessageCommand');

class GetNetworkInfo extends MessageCommand {
    constructor(networkEntryReference, commandTransceiver, responseParser) {
        super(commandTransceiver);
        this.responseParser = responseParser;
        this.networkEntryReference = networkEntryReference;
    }

    async invoke() {
        const reference = this.networkEntryReference;
        const parameters = { };
        for (const exchange of reference.iterateGetNetworkExchanges()) {
            await this.transmit(exchange.renderRequest());
            const reply = exchange.parseResponse(await this.receive());
            exchange.collect(reply, parameters);
        }
        return reference.makeNetworkInfo(parameters);
    }
}

module.exports = { GetNetworkInfo };