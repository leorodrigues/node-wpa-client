const { PayloadParameterArray } = require('./PayloadParameterArray');

class EventCommand {
    constructor(eventTransceiver, basicPayload, parameterArray) {
        this.eventTransceiver = eventTransceiver;
        this.parameterArray = parameterArray || new PayloadParameterArray();
        this.basicPayload = basicPayload;
    }

    async invoke() {
        return this.eventTransceiver.transmit(
            this.parameterArray.appendToPayload(this.basicPayload));
    }
}

module.exports = { EventCommand };