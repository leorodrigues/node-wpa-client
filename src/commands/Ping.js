
const { MessageCommand } = require('./MessageCommand');

class Ping extends MessageCommand {
    constructor(commandTransceiver, responseParser) {
        super(commandTransceiver);
        this.responseParser = responseParser;
    }

    async invoke() {
        await this.transmit('PING');
        const reply = await this.receive();
        return this.responseParser.parsePong(reply);
    }
}

module.exports = { Ping };