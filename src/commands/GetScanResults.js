
const { MessageCommand } = require('./MessageCommand');

class GetScanResults extends MessageCommand {
    constructor(messageTransceiver, responseParser) {
        super(messageTransceiver);
        this.responseParser = responseParser;
    }

    async invoke() {
        await this.transmit('SCAN_RESULTS');
        return this.responseParser.parseScanResult(await this.receive());
    }
}

module.exports = { GetScanResults };