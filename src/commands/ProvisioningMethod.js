
const { PeerPinNegotiationMethod } = require('./PeerPinNegotiationMethod');

class ProvisioningMethod {
    constructor(method, peerPinNegotiationMethod) {
        this.method = method;
        this.peerPinNegotiationMethod = peerPinNegotiationMethod
            || PeerPinNegotiationMethod.NONE;
    }

    appendToPayload(payloadString) {
        payloadString = `${payloadString} ${this.method}`;
        return this.peerPinNegotiationMethod.appendToPayload(payloadString);
    }

    static get PBC() {
        return PROVISIONING_METHOD_PBC;
    }

    static get PIN() {
        return PROVISIONING_METHOD_PIN;
    }

    static get LABEL_PIN() {
        return PROVISIONING_METHOD_LABEL_PIN;
    }

    static get DISPLAY_PIN() {
        return PROVISIONING_METHOD_DISPLAY_PIN;
    }

    static get KEYPAD_PIN() {
        return PROVISIONING_METHOD_KEYPAD_PIN;
    }
}

const PROVISIONING_METHOD_PBC =
    new ProvisioningMethod('pbc');

const PROVISIONING_METHOD_PIN =
    new ProvisioningMethod('pin');

const PROVISIONING_METHOD_LABEL_PIN =
    new ProvisioningMethod('PIN#', PeerPinNegotiationMethod.LABEL);

const PROVISIONING_METHOD_DISPLAY_PIN =
    new ProvisioningMethod('PIN#', PeerPinNegotiationMethod.DISPLAY);

const PROVISIONING_METHOD_KEYPAD_PIN =
    new ProvisioningMethod('PIN#', PeerPinNegotiationMethod.KEYPAD);

module.exports = { ProvisioningMethod };