const { PathResolver } = require('./PathResolver');
const { WpaClient } = require('./WpaClient');

const {
    CommandKit,
    ResponseParser,
    EventRegistrar,
    WpaEventParser
} = require('./commands');

const { TransceiverKit } = require('./communication');

const DEFAULT_TRANSCEIVER_KIT = new TransceiverKit();

/* istanbul ignore next */
class WpaClientKit {

    constructor(transceiverKit = DEFAULT_TRANSCEIVER_KIT) {
        this.transceiverKit = transceiverKit;
    }

    makeCommandTransceiver() {
        return this.transceiverKit.makeCommandTransceiver();
    }

    makeMonitorTransceiver(eventParser) {
        return this.transceiverKit.makeMonitorTransceiver(eventParser);
    }

    makeEventParser() {
        return new WpaEventParser();
    }

    makePathResolver(interfaceName, controlId) {
        return new PathResolver(interfaceName, controlId);
    }

    makeResponseParser() {
        return new ResponseParser();
    }

    makeEventRegistrar(monitorTransceiver) {
        return new EventRegistrar(monitorTransceiver);
    }

    makeCommandKit(
        commandTransceiver,
        monitorTransceiver,
        eventRegistrar,
        responseParser) {

        return new CommandKit(
            commandTransceiver,
            monitorTransceiver,
            eventRegistrar,
            responseParser);
    }

    makeWpaClient(pathResolver, commandKit) {
        return new WpaClient(pathResolver, commandKit);
    }
}

module.exports = { WpaClientKit };