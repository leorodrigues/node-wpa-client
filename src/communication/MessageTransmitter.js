
class MessageTransmitter {
    constructor(socket) {
        this.socket = socket;
    }

    transmit(message) {
        return new Promise((resolve, reject) => {
            const handleError = error => {
                this.socket.removeListener('error', handleError);
                reject(error);
            };

            const handleSuccess = error => {
                if (error) return handleError(error);
                this.socket.removeListener('error', handleError);
                resolve();
            };

            this.socket.once('error', handleError);
            this.socket.send(Buffer.from(message), handleSuccess);
        });
    }
}

module.exports = { MessageTransmitter };