
class MonitorTransceiver {
    constructor(eventReceiver, messageTransmitter, socketLifeCycle) {
        this.eventReceiver = eventReceiver;
        this.socketLifeCycle = socketLifeCycle;
        this.messageTransmitter = messageTransmitter;
    }

    on(event, callback) {
        this.eventReceiver.on(event, callback);
    }

    standByForEvents() {
        this.eventReceiver.prime();
    }

    transmit(message) {
        return this.messageTransmitter.transmit(message);
    }

    connect(serverPath) {
        return this.socketLifeCycle.connect(serverPath);
    }

    bind(localPath) {
        return this.socketLifeCycle.bind(localPath);
    }

    close() {
        return this.socketLifeCycle.close();
    }
}

module.exports = { MonitorTransceiver };