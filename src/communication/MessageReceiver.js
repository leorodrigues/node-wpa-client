
class MessageReceiver {
    constructor(socket) {
        this.socket = socket;
        this.outstandingMessages = [ ];
        this.outstandingResolutions = [ ];
    }

    receive() {
        if (this.outstandingMessages.length > 0)
            return Promise.resolve(this.outstandingMessages.shift());
        const promise = new Promise((resolve) => {
            this.outstandingResolutions.push(resolve);
        });
        return promise;
    }

    prime() {
        this.socket.on('message', data => {
            const message = data.toString();
            if (this.outstandingResolutions.length > 0) {
                const resolve = this.outstandingResolutions.shift();
                resolve(message);
                return;
            }
            this.outstandingMessages.push(message);
        });
    }
}

module.exports = { MessageReceiver };