
class SocketLifeCycle {
    constructor(socket) {
        this.socket = socket;
    }

    connect(serverPath) {
        return new Promise((resolve, reject) => {
            const handleError = error => {
                this.socket.removeListener('error', handleError);
                reject(error);
            };

            this.socket.once('connect', () => {
                this.socket.removeListener('error', handleError);
                resolve(this);
            });

            this.socket.once('error', handleError);
            this.socket.connect(serverPath);
        });
    }

    bind(localPath) {
        return new Promise((resolve, reject) => {
            const handleError = error => {
                this.socket.removeListener('error', handleError);
                reject(error);
            };

            this.socket.once('listening', () => {
                this.socket.removeListener('error', handleError);
                resolve(this);
            });

            this.socket.once('error', handleError);
            this.socket.bind(localPath);
        });
    }

    close() {
        return new Promise(resolve => {
            this.socket.close();
            resolve();
        });
    }
}

module.exports = { SocketLifeCycle };