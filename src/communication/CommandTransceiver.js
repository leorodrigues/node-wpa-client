
class CommandTransceiver {
    constructor(messageReceiver, messageTransmitter, socketLifeCycle) {
        this.socketLifeCycle = socketLifeCycle;
        this.messageReceiver = messageReceiver;
        this.messageTransmitter = messageTransmitter;
    }

    transmit(message) {
        return this.messageTransmitter.transmit(message);
    }

    receive() {
        return this.messageReceiver.receive();
    }

    connect(serverPath) {
        return this.socketLifeCycle.connect(serverPath);
    }

    bind(localPath) {
        const conclude = () => this.messageReceiver.prime();
        return this.socketLifeCycle.bind(localPath).then(conclude);
    }

    close() {
        return this.socketLifeCycle.close();
    }
}

module.exports = { CommandTransceiver };