
const { CommandTransceiver } = require('./CommandTransceiver');
const { MonitorTransceiver } = require('./MonitorTransceiver');

const { CommunicationKit } = require('./CommunicationKit');

const DEFAULT_COMMUNICATION_KIT = new CommunicationKit();

class TransceiverKit {

    /** istanbul ignore next */
    constructor(communicationKit = DEFAULT_COMMUNICATION_KIT) {
        this.communicationKit = communicationKit;
    }

    makeCommandTransceiver() {
        const socket = this.communicationKit.makeSocket();
        return new CommandTransceiver(
            this.communicationKit.makeMessageReceiver(socket),
            this.communicationKit.makeMessageTransmitter(socket),
            this.communicationKit.makeSocketLifeCycle(socket));
    }

    makeMonitorTransceiver(eventParser) {
        const socket = this.communicationKit.makeSocket();
        return new MonitorTransceiver(
            this.communicationKit.makeEventReceiver(socket, eventParser),
            this.communicationKit.makeMessageTransmitter(socket),
            this.communicationKit.makeSocketLifeCycle(socket));
    }
}

module.exports = { TransceiverKit };