
const { EventEmitter } = require('events');

class EventReceiver extends EventEmitter {
    constructor(socket, eventParser, options) {
        super(options);
        this.socket = socket;
        this.eventParser = eventParser;
    }

    prime() {
        this.socket.on('message', data => {
            const message = data.toString();
            const eventName = this.eventParser.parseName(message);
            const eventData = this.eventParser.parseData(message);
            this.emit(eventName, eventData);
        });
    }
}

module.exports = { EventReceiver };