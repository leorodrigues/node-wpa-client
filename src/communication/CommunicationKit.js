
const unix = require('unix-dgram');

const { MessageTransmitter } = require('./MessageTransmitter');
const { MessageReceiver } = require('./MessageReceiver');
const { SocketLifeCycle } = require('./SocketLifeCycle');
const { EventReceiver } = require('./EventReceiver');

/** istanbul ignore next */
class CommunicationKit {
    makeEventReceiver(socket, eventParser) {
        return new EventReceiver(socket, eventParser);
    }

    makeMessageReceiver(socket) {
        return new MessageReceiver(socket);
    }

    makeMessageTransmitter(socket) {
        return new MessageTransmitter(socket);
    }

    makeSocketLifeCycle(socket) {
        return new SocketLifeCycle(socket);
    }

    makeSocket() {
        return unix.createSocket('unix_dgram');
    }
}

module.exports = { CommunicationKit };