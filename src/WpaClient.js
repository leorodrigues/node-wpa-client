
class WpaClient {

    constructor(pathResolver, commandKit) {
        this.pathResolver = pathResolver;
        this.commandKit = commandKit;
    }

    async open() {
        const cmmdSockPath = this.pathResolver.getServerSocketPath();

        await this.commandKit.makeOpenCommandTransceiver(
            cmmdSockPath, this.pathResolver.getControlSocketPath('1')).invoke();

        await this.commandKit.makeOpenMonitorTransceiver(
            cmmdSockPath, this.pathResolver.getControlSocketPath('2')).invoke();

        return this.commandKit.makeAttach().invoke();
    }

    async close() {
        await this.commandKit.makeDetach(). invoke();
        await this.commandKit.makeCloseMonitorTransceiver().invoke();
        return this.commandKit.makeCloseCommandTransceiver().invoke();
    }

    /* istanbul ignore next */
    async standByForEvents(configure) {
        const cmd = this.commandKit.makeStandByForEvents(this, configure);
        return await cmd.invoke();
    }

    /* istanbul ignore next */
    async ping() {
        return this.commandKit.makePing().invoke();
    }

    /* istanbul ignore next */
    async listInterfaces() {
        return this.commandKit.makeListInterfaces().invoke();
    }

    /* istanbul ignore next */
    async scan(period) {
        return this.commandKit.makeScan(period).invoke();
    }

    /* istanbul ignore next */
    async getScanResults() {
        return this.commandKit.makeGetScanResults().invoke();
    }

    /* istanbul ignore next */
    async getStatus() {
        return this.commandKit.makeGetStatus().invoke();
    }

    /* istanbul ignore next */
    async listNetworkEntries() {
        return this.commandKit.makeListNetworkEntries().invoke();
    }

    /* istanbul ignore next */
    async addNetworkEntry() {
        return this.commandKit.makeAddNetworkEntry().invoke();
    }

    /* istanbul ignore next */
    async patchNetworkEntry(networkInfo) {
        return this.commandKit.makePatchNetworkEntry(networkInfo).invoke();
    }

    /* istanbul ignore next */
    async selectNetworkEntry(networkEntryReference) {
        return this.commandKit.makeSelectNetworkEntry(
            networkEntryReference).invoke();
    }

    /* istanbul ignore next */
    async enableNetworkEntry(networkEntryReference) {
        return this.commandKit.makeEnableNetworkEntry(
            networkEntryReference).invoke();
    }

    /* istanbul ignore next */
    async disableNetworkEntry(networkEntryReference) {
        return this.commandKit.makeDisableNetworkEntry(
            networkEntryReference).invoke();
    }

    /* istanbul ignore next */
    async removeNetworkEntry(networkEntryReference) {
        return this.commandKit.makeRemoveNetworkEntry(
            networkEntryReference);
    }

    /* istanbul ignore next */
    async getNetworkInfo(networkEntryReference) {
        return this.commandKit.makeGetNetworkInfo(
            networkEntryReference).invoke();
    }

    /* istanbul ignore next */
    async disconnect() {
        return this.commandKit.makeDisconnect().invoke();
    }

    /* istanbul ignore next */
    async reassociate() {
        return this.commandKit.makeReassociate().invoke();
    }

    /* istanbul ignore next */
    async reconnect() {
        return this.commandKit.makeReconnect().invoke();
    }

    async p2pAddGroup() {
        return this.commandKit.makeP2PAddGroup().invoke();
    }

    async p2pRemoveGroup() {
        return this.commandKit.makeP2PRemoveGroup().invoke();
    }

    async p2pRequestProvisionDicovery(peerAddress) {
        return this.commandKit
            .makeP2PRequestProvisioningDiscovery(peerAddress).invoke();
    }

    /* istanbul ignore next */
    async p2pFind(period, type) {
        return this.commandKit.makeP2PFind(period, type).invoke();
    }

    /* istanbul ignore next */
    async p2pStopFinding() {
        return this.commandKit.makeP2PStopFinding().invoke();
    }

    /* istanbul ignore next */
    async p2pConnect(peerConnectionParams) {
        return this.commandKit.makeP2PConnect(peerConnectionParams).invoke();
    }

    /* istanbul ignore next */
    async p2pInfo(peerReference) {
        return this.commandKit.makeP2PInfo(peerReference).invoke();
    }

    /* istanbul ignore next */
    async p2pInvite() {
        return this.commandKit.makeP2PInvite().invoke();
    }

    async p2pListen(period) {
        return this.commandKit.makeP2PListen(period).invoke();
    }
}

module.exports = { WpaClient };