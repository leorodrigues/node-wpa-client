
import * as Communication from './communication';

export declare class StatusResult {

}

/**
 * This class abstracts an amount of time in seconds.
 */
export declare class Period {
    /**
     * An endless amount of time.
     */
    static UNKNOWN: Period;

    /**
     * Instantiates a new `Period`.
     * @param value A real number.
     */
    constructor(value: number);

    /**
     * Will suspend execution during this period or not at all, in case the
     * current instance is the `Period.UNKNOWN`.
     */
    holdOn(): Promise<void>;

    /**
     * Will apply the internal value to the given template.
     * @param template A string probably containing the `:value` placeholder.
     */
    interpolate(template: string): string;
}

/**
 * This class holds the scan record data from a `WpaClient.scan(period)` call.
 * @see WpaClient#scan
 */
export declare class ScanResult {

    /**
     * Instantiates a new `ScanResult`.
     * @param bssid The identification of the service set.
     * @param frequency The frequency of operation used by the network.
     * @param signalLevel The level of signal.
     * @param flags Overall configuration flags.
     * @param ssid The "network name".
     */
    constructor(
        bssid: string,
        frequency: number,
        signalLevel: number,
        flags: string[],
        ssid: string);
}

/**
 * Each instance of this class holds one list record from a
 * `WpaClient.listNetworkEntries()` call.
 * @see WpaClient#listNetworkEntries
 */
export declare class NetworkEntry {

    /**
     * Instantiates a new `NetworkEntry` object.
     * @param networkEntryReference A reference to some point in the list.
     * @param ssid The "name" of the network.
     * @param bssid Basic Service Set Identification.
     * @param flags Configuration flags.
     */
    constructor(
        networkEntryReference: NetworkEntryReference,
        ssid: string,
        bssid: string,
        flags: string[]);
}

export declare class NetworkInfo {
    constructor(
        ssid?: string,
        psk?: string,
        keyManagement?: string,
        identity?: string,
        password?: string);
    iterateSetPayloads(): Iterator<string>;
}

export declare interface IMessageFactory {
    makePing(): string;
    makeAttach(): string;
    makeListInterfaces(): string;
    makeScan(period: Period): string;
}

export declare interface IResponseParser {
    parseInt(rawData: string): number;
    parseOk(rawData: string): string;
    parsePong(rawData: string): string;
    parseListInterfaces(rawData: string): string[];
    parseScanResults(rawData: string): ScanResult[];
    parseListResults(rawData: string): NetworkEntry[];
}

export declare interface IEventRegistrar {
    onP2PDeviceFound(handle: IRegistrarEventCallback): void;
    onP2PDeviceLost(handle: IRegistrarEventCallback): void;
    onP2PFindStopped(handle: IRegistrarEventCallback): void;
    onScanStarted(handle: IRegistrarEventCallback): void;
}

export declare interface ICommand {
    invoke(): Promise<void>;
}

export declare class WpaEventParser implements Communication.IEventParser {
    parseName(message: string): string;
    parseData(message: string): any;
}

export declare class MessageFactory implements IMessageFactory {
    makePing(): string;
    makeAttach(): string;
    makeListInterfaces(): string;
    makeScan(period: Period): string;
}

export declare class ResponseParser implements IResponseParser {
    parseInt(rawData: string): number;
    parseOk(rawData: string): string;
    parsePong(rawData: string): string;
    parseListInterfaces(rawData: string): string[];
    parseScanResults(rawData: string): ScanResult[];
    parseListResults(rawData: string): NetworkEntry[];
}

export declare interface IRegistrarEventCallback {
    (...args: any[]): void;
}

export declare class EventRegistrar {
    constructor(monitorTransceiver: Communication.IEventTransceiver);
    onP2PDeviceFound(handle: IRegistrarEventCallback): void;
    onP2PDeviceLost(handle: IRegistrarEventCallback): void;
    onP2PFindStopped(handle: IRegistrarEventCallback): void;
    onScanStarted(handle: IRegistrarEventCallback): void;
}

export declare class SocketLifeCycleCommand implements ICommand {
    constructor(socketLifeCycle: Communication.ISocketLifeCycle);
    invoke(): Promise<void>;
}

export declare class EventCommand implements ICommand {
    constructor(eventTransceiver: Communication.IEventTransceiver);
    transmit(message: string): Promise<void>;
    invoke(): Promise<void>;
}

export declare class MessageCommand implements ICommand {
    constructor(messageTransceiver: Communication.IMessageTransceiver);
    transmit(message: string): Promise<void>;
    receive(): Promise<void>;
    invoke(): Promise<void>;
}

export declare class OpenTransceiver extends SocketLifeCycleCommand {

    constructor(
        socketLifeCycle: Communication.ISocketLifeCycle,
        serverSocketPath: string,
        clientSocketPath: string);

    invoke(): Promise<void>;
}

/**
 * This is a command class that will close a transceiver.
 */
export class CloseTransceiver extends SocketLifeCycleCommand {

    /**
     * It instantiates a new `CloseTransceiver` command.
     * @param transceiver Any instance of transceiver.
     */
    constructor(socketLifeCycle: Communication.ISocketLifeCycle);

    /**
     * It will just return the call to the close method
     * of the given transceiver.
     */
    invoke(): Promise<void>;
}

/**
 * This is a "fire and forget" type of command. It is used to,
 * well as the name suggests, attach a socket to the `wpa_supplicant` daemon,
 * allowing a process to monitor the daemon and receive events from it.
 * @see Communications/MonitorTransceiver
 */
export class Attach extends EventCommand {

    /**
     * It instantiates a new `Attach` command.
     * @param eventTransceiver
     * A `Communication.IEventTransceiver` instance.
     * @param messageFactory A `MessageFactory` instance.
     * @param responseParser A `ResponseParser` instance.
     */
    constructor(
        eventTransceiver: Communication.IEventTransceiver,
        messageFactory: IMessageFactory,
        responseParser: IResponseParser);

    /**
     * It transmits the `ATTACH` wpa command via `monitorTransceiver`
     * and resolves the promise as soon as the transmission finishes.
     * 
     * It is recomended that you configure the given monitor transceiver,
     * binding event handlers to it, BEFORE invoking this method.
     * If you attach first and THEN bind the handlers, you will risk missing out
     * some of the events emitted by the monitor.
     * @returns A void promise.
     */
    invoke(): Promise<void>;
}

/**
 * This command will detach the transceiver socket from the `wpa_supplicant`
 * daemon.
 * @see Communication/MonitorTransceiver
 */
export class Detach extends EventCommand {

    /**
     * It instantiates a new `Detach` command.
     * @param eventTransceiver
     * A `Communication.IEventTransceiver` instance.
     */
    constructor(eventTransceiver: Communication.IEventTransceiver);

    /**
     * It transmits the `DETACH` wpa command via `monitorTransceiver`
     * and resolves the promise as soon as the transmission finishes.
     * 
     * After this command is sent, the target transceiver will stop receiving
     * events from the `wpa_supplicant` daemon.
     * @returns A void promise.
     */
    invoke(): Promise<void>;
}

export class Ping {
    constructor(
        messageTransceiver: Communication.IMessageTransceiver,
        messageFactory: IMessageFactory,
        responseParser: IResponseParser);
    invoke(): Promise<boolean>;
}

export class Scan {
    constructor(
        period: Period,
        messageTransceiver: Communication.IMessageTransceiver,
        messageFactory: IMessageFactory,
        responseParser: IResponseParser);
    invoke(): Promise<ScanResult[]>;
}

export class ListNetworkEntries {
    constructor(
        messageTransceiver: Communication.IMessageTransceiver,
        responseParser: IResponseParser);
    invoke(): Promise<NetworkEntry[]>;
}

export class NetworkEntryFabricationParams {
    public ssid?: string;
    public bssid?: string;
    public flags?: string;
}

export class NetworkInfoFabricationParams {
    public ssid?: string;
    public psk?: string;
    public keyMgmt?: string;
    public identity?: string;
    public password?: string;
}

export class NetworkEntryReference {
    constructor(id: number);

    get enableNetworkCmdString(): string;
    get disableNetworkCmdString(): string;
    get selectNetworkCmdString(): string;

    getCommands(): Iterable<string>;

    makeNetworkEntry(params: NetworkEntryFabricationParams): NetworkEntry;
    makeNetworkInfo(params: NetworkInfoFabricationParams): NetworkInfo;
}

export class DisableNetworkEntry {
    constructor(
        networkEntryReference: NetworkEntryReference,
        commandTransceiver: Communication.IMessageTransceiver,
        responseParser: IResponseParser);
    invoke(): Promise<void>;
}

export class EnableNetworkEntry {
    constructor(
        networkEntryReference: NetworkEntryReference,
        commandTransceiver: Communication.IMessageTransceiver,
        responseParser: IResponseParser);
    invoke(): Promise<void>;
}

export class SelectNetworkEntry {
    /**
     * It instanciates a new `AddNetworkEntry` command.
     * @param networkEntryReference A reference to a list entry as returned
     * from `listNetworkEntries` command.
     * @param commandTransceiver A `CommandTransceiver`.
     * @param responseParser A `ResponseParser`.
     */
    constructor(
        networkEntryReference: NetworkEntryReference,
        commandTransceiver: Communication.IMessageTransceiver,
        responseParser: IResponseParser);

    /**
      * It transmits the `SELECT_NETWORK` command to the `wpa_supplicant`.
      * 
     * @returns A void promise.
     */
    invoke(): Promise<void>;
}

/**
 * This is a command class that will create a new network entry.
 */
export class AddNetworkEntry {
    /**
     * It instanciates a new `AddNetworkEntry` command.
     * @param commandTransceiver A `CommandTransceiver`.
     * @param responseParser A `ResponseParser`.
     */
    constructor(
        commandTransceiver: Communication.IMessageTransceiver,
        responseParser: IResponseParser);

    /**
      * It transmits the `ADD_NETWORK` command to the `wpa_supplicant` then
      * enters in receive mode, expecting an integer id as reply. Such id is
      * a positional index in a list which starts at zero. It is also
      * encapsulated on a `NetworkEntryReference` instance.
      * 
     * @returns A promise to the network reference on the list.
     */
    invoke(): Promise<NetworkEntryReference>;
}

/**
 * The `CommandKit` abstract factory is used by the `WpaClient` to instantiate
 * all the actual command instances, to which it delegates its own interface
 * calls.
 * 
 * One should extend this class and override some of the construction methods
 * if one wishes to use customized versions of the provided commands.
 */
export class CommandKit {

    /**
     * Instantiates a new `CommandKit` object.
     * @param commandTransceiver A `CommandTransceiver` instance.
     * @param messageFactory A `MessageFactory` instance.
     * @param responseParser A `ResponseParser` instance.
     */
    constructor(
        commandTransceiver: Communication.IMessageTransceiver,
        messageFactory: IMessageFactory,
        responseParser: IResponseParser);

    /**
     * Creates a new `OpenTransceiver` command, referencing the command
     * transceiver instance.
     * @param serverSocketPath The path to one of the `wpa_supplicant`
     * configured interface sockets.
     * @param controlSocketPath The path to the soon-to-be-created control
     * socket.
     * @returns The newly created command instance.
     */
    makeOpenCommandTransceiver(
        serverSocketPath: string,
        controlSocketPath: string): OpenTransceiver;

    /**
     * Creates a new `OpenTransceiver` command, referencing the monitor
     * transceiver instance.
     * @param serverSocketPath The path to one of the `wpa_supplicant`
     * configured interface sockets.
     * @param controlSocketPath The path to the soon-to-be-created control
     * socket.
     * @returns The newly created command instance.
     */
    makeOpenMonitorTransceiver(
        serverSocketPath: string,
        controlSocketPath: string): OpenTransceiver;

    /**
     * Creates a new `Close` command.
     * @returns The newly created command instance.
     */
    makeClose(): CloseTransceiver;

    /**
     * Creates a new `Ping` command.
     * @returns The newly created command instance.
     */
    makePing(): Ping;

    /**
     * Creates a new `Attach` command.
     * @returns The newly created command instance.
     */
    makeAttach(): Attach;

    /**
     * Creates a new `Scan` command.
     * @param period The amount of seconds the scan should take.
     * @returns The newly created command instance.
     */
    makeScan(period: Period): Scan;

    /**
     * Creates a new `ListNetworkEntries` command.
     * @returns The newly created command instance.
     */
    makeListNetworkEntries(): ListNetworkEntries;

    /**
     * Creates a new `AddNetworkEntry` command.
     * @param network A network configuration instance.
     * @returns The newly created command instance.
     */
    makeAddNetWork(network: NetworkInfo): AddNetworkEntry;
}