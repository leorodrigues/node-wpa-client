const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const { Commands: { CloseTransceiver } } = require('../../index');

const { fakeCommandTransceiver } = makeMocks(sinon.createSandbox());

const subject = new CloseTransceiver(fakeCommandTransceiver);

describe('Commands/CloseTransceiver', () => {
    describe('async invoke()', () => {
        it('Should delegate to the `commandTransceiver` underneath', async () => {
            fakeCommandTransceiver.close.returns('fake return');
            expect(await subject.invoke()).to.be.equal('fake return');
        });
    });
});