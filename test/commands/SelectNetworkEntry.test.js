const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const {
    SelectNetworkEntry,
    NetworkEntryReference
} = require('../../src/commands');


const {
    fakeCommandTransceiver,
    fakeResponseParser
} = makeMocks(sinon.createSandbox());

describe('Commands/SelectNetworkEntry', () => {
    describe('async invoke()', () => {
        it('Should transmit "SELECT_NETWORK 11", receive "OK" and return true.', async () => {
            fakeResponseParser.parseOk.returns('OK');
            fakeCommandTransceiver.transmit.resolves();
            fakeCommandTransceiver.receive.resolves('OK\n');

            const reference = new NetworkEntryReference(11);
            const subject = new SelectNetworkEntry(
                reference, fakeCommandTransceiver, fakeResponseParser);

            expect(await subject.invoke()).to.be.true;

            expect(fakeCommandTransceiver.transmit)
                .to.have.been.calledWith('SELECT_NETWORK 11');

            expect(fakeCommandTransceiver.receive)
                .to.have.been.calledWith();
        });
    });
});