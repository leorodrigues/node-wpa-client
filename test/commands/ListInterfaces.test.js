const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const {
    fakeCommandTransceiver,
    fakeResponseParser
} = makeMocks(sinon.createSandbox());

const { ListInterfaces } = require('../../src/commands');

describe('Commands/ListInterfaces', () => {
    describe('async invoke()', () => {

        it('Should successfully fire up the command.', async () => {
            fakeCommandTransceiver.transmit.resolves();
            fakeCommandTransceiver.receive.resolves('fake-reply');
            fakeResponseParser.parseListInterfaces.returns('fake-list');

            const subject = new ListInterfaces(
                fakeCommandTransceiver, fakeResponseParser);

            expect(await subject.invoke()).to.be.equal('fake-list');

            expect(fakeCommandTransceiver.transmit)
                .to.have.been.calledOnceWithExactly('INTERFACES');

            expect(fakeCommandTransceiver.receive)
                .to.have.been.calledOnceWithExactly();

            expect(fakeResponseParser.parseListInterfaces)
                .to.have.been.calledOnceWithExactly('fake-reply');
        });
    });
});