const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const { ListNetworkEntries } = require('../../src/commands');

const {
    fakeCommandTransceiver,
    fakeResponseParser
} = makeMocks(sinon.createSandbox());

describe('Commands/ListNetworkEntries', () => {
    describe('async invoke()', () => {
        it('Should transmit "LIST_NETWORKS", receive a payload and parse.', async () => {
            fakeResponseParser.parseNetworkEntries.returns('huge-list-of-entries');
            fakeCommandTransceiver.transmit.resolves();
            fakeCommandTransceiver.receive.resolves('huge-response-payload');

            const subject = new ListNetworkEntries(
                fakeCommandTransceiver, fakeResponseParser);

            expect(await subject.invoke())
                .to.be.equal('huge-list-of-entries');

            expect(fakeCommandTransceiver.transmit)
                .to.have.been.calledWith('LIST_NETWORKS');

            expect(fakeCommandTransceiver.receive)
                .to.have.been.calledWith();

            expect(fakeResponseParser.parseNetworkEntries)
                .to.have.been.calledWith('huge-response-payload');
        });
    });
});