const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const { fakeCommandTransceiver } = makeMocks(sinon.createSandbox());

const {
    ResponseParser,
    AddNetworkEntry,
    NetworkEntryReference
} = require('../../src/commands');

describe('Commands/AddNetworkEntry', () => {
    describe('async invoke()', () => {
        it('Should transmit ADD_NETWORK, receive an integer and return a reference.', async () => {
            fakeCommandTransceiver.transmit.resolves();
            fakeCommandTransceiver.receive.resolves('1\n');
            
            const subject = new AddNetworkEntry(fakeCommandTransceiver, new ResponseParser());
            const result = await subject.invoke();

            expect(result)
                .to.be.instanceOf(NetworkEntryReference);

            expect(fakeCommandTransceiver.transmit)
                .to.have.been.calledWith('ADD_NETWORK');

            expect(fakeCommandTransceiver.receive)
                .to.have.been.calledWith();
        });
    });
});