
const chai = require('chai');
const { expect } = chai;
const { GroupFormationDirective } =
    require('../../src/commands/GroupFormationDirective');

describe('Commands/GroupFormationDirective', () => {
    describe('.NONE#appendToPayload(payloadString)', () => {
        it('Should append nothing', () => {
            expect(GroupFormationDirective.NONE.appendToPayload('fake-payload'))
                .to.be.equal('fake-payload');
        });
    });

    describe('.JOIN#appendToPayload(payloadString)', () => {
        it('Should append "join" to the end of the payload', () => {
            expect(GroupFormationDirective.JOIN.appendToPayload('fake-payload'))
                .to.be.equal('fake-payload join');
        });
    });

    describe('.JOIN_AND_PERSIST#appendToPayload(payloadString)', () => {
        it('Should append "join" to the end of the payload', () => {
            expect(GroupFormationDirective.JOIN_AND_PERSIST.appendToPayload('fake-payload'))
                .to.be.equal('fake-payload persistent join');
        });
    });

    describe('.AUTH#appendToPayload(payloadString)', () => {
        it('Should append "auth" to the end of the payload', () => {
            expect(GroupFormationDirective.AUTH.appendToPayload('fake-payload'))
                .to.be.equal('fake-payload auth');
        });
    });

    describe('.AUTH_AND_PERSIST#appendToPayload(payloadString)', () => {
        it('Should append "persistent join" to the end of the payload', () => {
            expect(GroupFormationDirective.AUTH_AND_PERSIST.appendToPayload('fake-payload'))
                .to.be.equal('fake-payload persistent auth');
        });
    });
});