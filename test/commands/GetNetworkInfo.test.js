const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const { fakeCommandTransceiver, sandbox } = makeMocks(sinon.createSandbox());

const {
    NetworkInfo,
    ResponseParser,
    GetNetworkInfo,
    NetworkEntryReference
} = require('../../src/commands');

describe('Commands/GetNetworkInfo', () => {
    describe('async invoke()', () => {
        afterEach(() => sandbox.reset());

        it('Should perform several successful GET_NETWORK exchanges.', async () => {
            const reference = new NetworkEntryReference(11);
            const expectedInfo = new NetworkInfo(
                reference,
                'test network',
                'test secret',
                'test flags',
                'test identity',
                'test password'
            );

            fakeCommandTransceiver.transmit.resolves();

            fakeCommandTransceiver.receive
                .onCall(0).resolves('test network\n');

            fakeCommandTransceiver.receive
                .onCall(1).resolves('test secret\n');

            fakeCommandTransceiver.receive
                .onCall(2).resolves('test flags\n');

            fakeCommandTransceiver.receive
                .onCall(3).resolves('test identity\n');

            fakeCommandTransceiver.receive
                .onCall(4).resolves('test password\n');

            const subject = new GetNetworkInfo(
                reference, fakeCommandTransceiver, new ResponseParser());

            const result = await subject.invoke();

            expect(result).to.be.instanceOf(NetworkInfo);

            expect(result.equals(expectedInfo)).to.be.true;

            expect(fakeCommandTransceiver.transmit)
                .to.have.callCount(5)
                .to.have.been.calledWith('GET_NETWORK 11 ssid')
                .to.have.been.calledWith('GET_NETWORK 11 psk')
                .to.have.been.calledWith('GET_NETWORK 11 key_mgmt')
                .to.have.been.calledWith('GET_NETWORK 11 identity')
                .to.have.been.calledWith('GET_NETWORK 11 password');

            expect(fakeCommandTransceiver.receive)
                .to.have.callCount(5);
        });

        it('Should fail to get the "psk" for the network.', async () => {
            const reference = new NetworkEntryReference(11);
            const expectedInfo = new NetworkInfo(
                reference,
                'test network',
                'test secret',
                undefined,
                'test identity',
                'test password'
            );

            fakeCommandTransceiver.transmit.resolves();

            fakeCommandTransceiver.receive
                .onCall(0).resolves('test network\n');

            fakeCommandTransceiver.receive
                .onCall(1).resolves('test secret\n');

            fakeCommandTransceiver.receive
                .onCall(2).resolves('FAIL\n');

            fakeCommandTransceiver.receive
                .onCall(3).resolves('test identity\n');

            fakeCommandTransceiver.receive
                .onCall(4).resolves('test password\n');

            const subject = new GetNetworkInfo(
                reference, fakeCommandTransceiver, new ResponseParser());

            const result = await subject.invoke();

            expect(result).to.be.instanceOf(NetworkInfo);

            expect(result.equals(expectedInfo)).to.be.true;

            expect(fakeCommandTransceiver.transmit)
                .to.have.callCount(5)
                .to.have.been.calledWith('GET_NETWORK 11 ssid')
                .to.have.been.calledWith('GET_NETWORK 11 psk')
                .to.have.been.calledWith('GET_NETWORK 11 key_mgmt')
                .to.have.been.calledWith('GET_NETWORK 11 identity')
                .to.have.been.calledWith('GET_NETWORK 11 password');

            expect(fakeCommandTransceiver.receive)
                .to.have.callCount(5);
        });
    });
});