const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const { PatchNetworkEntry } = require('../../src/commands');

const {
    fakeCommandTransceiver,
    fakeResponseParser,
    sandbox
} = makeMocks(sinon.createSandbox());

const fakeNetworkInfo = { iterateSetPayloads: sandbox.stub() };

describe('Commands/PatchNetworkEntry', () => {
    describe('async invoke()', () => {
        it('Should transmit all commands and confirm all replies.', async () => {
            fakeNetworkInfo.iterateSetPayloads.returns(['a', 'b', 'c']);
            fakeResponseParser.parseOk.returns('OK');
            fakeCommandTransceiver.transmit.resolves();
            fakeCommandTransceiver.receive.resolves('reply-payload');

            const subject = new PatchNetworkEntry(
                fakeNetworkInfo, fakeCommandTransceiver, fakeResponseParser);

            expect(await subject.invoke())
                .to.be.equal(fakeNetworkInfo);

            expect(fakeCommandTransceiver.transmit)
                .to.have.been.calledThrice
                .to.have.been.calledWith('a')
                .to.have.been.calledWith('b')
                .to.have.been.calledWith('c');

            expect(fakeCommandTransceiver.receive)
                .to.be.calledThrice
                .to.have.been.calledWith();

            expect(fakeResponseParser.parseOk)
                .to.be.calledThrice
                .to.have.been.calledWith();
        });
    });
});