const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const { Commands: { Ping } } = require('../../index');

const {
    fakeCommandTransceiver,
    fakeResponseParser
} = makeMocks(sinon.createSandbox());

const subject = new Ping(fakeCommandTransceiver, fakeResponseParser);

describe('Commands/Ping', () => {
    describe('async invoke()', () => {
        it('Should `connect` and `bind` successfully', async () => {
            fakeCommandTransceiver.transmit.resolves();
            fakeCommandTransceiver.receive.resolves('some-data');
            fakeResponseParser.parsePong.returns('PONG');

            expect(await subject.invoke())
                .to.be.equal('PONG');

            expect(fakeCommandTransceiver.transmit)
                .to.have.been.calledWith('PING');

            expect(fakeResponseParser.parsePong)
                .to.have.been.calledWith('some-data');
        });
    });
});