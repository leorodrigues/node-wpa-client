const chai = require('chai');

const { expect } = chai;

chai.use(require('sinon-chai'));

const {
    Commands: { NetworkEntry, NetworkEntryReference }
} = require('../../index');

const parameters = {
    ssid: 'fake-ssid',
    bssid: 'fake-bssid',
    flags: 'fake-flags'
};

describe('Commands/NetworkEntry', () => {
    describe('equals(another)', () => {
        it('Should be true if they are the exact same instance', () => {
            const subject = new NetworkEntry();
            expect(subject.equals(subject)).to.be.true;
        });

        it('Should be true if they have the same reference and same content', () => {
            const left = NetworkEntry.makeInstance({
                reference: new NetworkEntryReference(7), ...parameters
            });

            const right = NetworkEntry.makeInstance({
                reference: new NetworkEntryReference(7), ...parameters
            });

            expect(left.equals(right)).to.be.true;
        });

        it('Should be false if they are not the same entity', () => {
            const left = NetworkEntry.makeInstance({
                reference: new NetworkEntryReference(34), ...parameters
            });

            const right = NetworkEntry.makeInstance({
                reference: new NetworkEntryReference(41), ...parameters
            });

            expect(left.equals(right)).to.be.false;
        });

        it('Should be false if they don\'t have the same content', () => {
            const left = NetworkEntry.makeInstance({
                reference: new NetworkEntryReference(97), ...parameters, ssid: 'corrupt-value'
            });

            const right = NetworkEntry.makeInstance({
                reference: new NetworkEntryReference(97), ...parameters
            });

            expect(left.equals(right)).to.be.false;
        });
    });

    describe('hasTheSameContentsOf(another)', () => {
        it('Should be true if the objects are empty', () => {
            const left = NetworkEntry.makeInstance({ });
            const right = NetworkEntry.makeInstance({ });
            expect(left.hasTheSameContentsOf(right)).to.be.true;
        });
        it('Should be true if all attributes match', () => {
            const left = NetworkEntry.makeInstance(parameters);
            const right = NetworkEntry.makeInstance(parameters);
            expect(left.hasTheSameContentsOf(right)).to.be.true;
        });
        it('Should be false if SSIDs differ', () => {
            const left = NetworkEntry.makeInstance(parameters);
            const right = NetworkEntry.makeInstance({
                ...parameters, ssid: 'corrupt-value'
            });
            expect(left.hasTheSameContentsOf(right)).to.be.false;
        });
        it('Should be false if BSSIDs differ', () => {
            const left = NetworkEntry.makeInstance(parameters);
            const right = NetworkEntry.makeInstance({
                ...parameters, bssid: 'corrupt-value'
            });
            expect(left.hasTheSameContentsOf(right)).to.be.false;
        });
        it('Should be false if flags differ', () => {
            const left = NetworkEntry.makeInstance(parameters);
            const right = NetworkEntry.makeInstance({
                ...parameters, flags: 'corrupt-value'
            });
            expect(left.hasTheSameContentsOf(right)).to.be.false;
        });
    });

    describe('isTheSameEntityAs(another)', () => {
        it('Should be true if they have equivalent references', () => {
            const left = new NetworkEntry(new NetworkEntryReference(13));
            const right = new NetworkEntry(new NetworkEntryReference(13));
            expect(left.isTheSameEntityAs(right)).to.be.true;
        });

        it('Should be false if they have mismatching references', () => {
            const left = new NetworkEntry(new NetworkEntryReference(11));
            const right = new NetworkEntry(new NetworkEntryReference(19));
            expect(left.isTheSameEntityAs(right)).to.be.false;
        });

        it('Should be false if either one is missing its reference', () => {
            const left = new NetworkEntry();
            const right = new NetworkEntry(new NetworkEntryReference(19));
            expect(left.isTheSameEntityAs(right)).to.be.false;
            expect(right.isTheSameEntityAs(left)).to.be.false;
        });

        it('Should be false if the other one is not a NetworkEntry instance', () =>{
            expect(new NetworkEntry().isTheSameEntityAs({ })).to.be.false;
        });
    });
});

