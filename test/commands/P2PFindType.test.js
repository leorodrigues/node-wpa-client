const chai = require('chai');

const { expect } = chai;

chai.use(require('sinon-chai'));

const { Commands: { P2PFindType } } = require('../../index');

describe('Commands/P2PFindType', () => {
    describe('.NONE#appendToPayload(payloadString)', () => {
        it('Should not change the payload at all', () =>
            expect(P2PFindType.NONE.appendToPayload('fake-payload'))
                .to.be.equal('fake-payload'));
    });

    describe('.SOCIAL#appendToPayload(payloadString)', () => {
        it('Should append "label" to the end of the payload', () =>
            expect(P2PFindType.SOCIAL.appendToPayload('fake-payload'))
                .to.be.equal('fake-payload type=social'));
    });

    describe('.PROGRESSIVE#appendToPayload(payloadString)', () => {
        it('Should append "display" to the end of the payload', () =>
            expect(P2PFindType.PROGRESSIVE.appendToPayload('fake-payload'))
                .to.be.equal('fake-payload type=progressive'));
    });
});