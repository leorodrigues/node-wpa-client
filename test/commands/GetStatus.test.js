const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const {
    fakeCommandTransceiver,
    fakeResponseParser
} = makeMocks(sinon.createSandbox());

const { GetStatus } = require('../../src/commands');

describe('Commands/GetStatus', () => {
    describe('async invoke()', () => {

        it('Should successfully fire up the command.', async () => {
            fakeCommandTransceiver.transmit.resolves();
            fakeCommandTransceiver.receive.resolves('fake-reply');
            fakeResponseParser.parseStatus.returns('fake-status');

            const subject = new GetStatus(
                fakeCommandTransceiver, fakeResponseParser);

            expect(await subject.invoke()).to.be.equal('fake-status');

            expect(fakeCommandTransceiver.transmit)
                .to.have.been.calledOnceWithExactly('STATUS');

            expect(fakeCommandTransceiver.receive)
                .to.have.been.calledOnceWithExactly();

            expect(fakeResponseParser.parseStatus)
                .to.have.been.calledOnceWithExactly('fake-reply');
        });
    });
});