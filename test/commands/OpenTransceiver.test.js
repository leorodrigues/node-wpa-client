const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const { Commands: { OpenTransceiver } } = require('../../index');

const { fakeCommandTransceiver } = makeMocks(sinon.createSandbox());

const subject = new OpenTransceiver(fakeCommandTransceiver, 'server.sock', 'client.sock');

describe('Commands/OpenTransceiver', () => {
    describe('async invoke()', () => {
        it('Should `connect` and `bind` successfully', async () => {
            fakeCommandTransceiver.connect.resolves();
            fakeCommandTransceiver.bind.resolves();

            await subject.invoke();

            expect(fakeCommandTransceiver.connect)
                .to.have.been.calledWith('server.sock');

            expect(fakeCommandTransceiver.bind)
                .to.have.been.calledWith('client.sock');
        });
    });
});