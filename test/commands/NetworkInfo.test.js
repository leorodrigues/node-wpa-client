const chai = require('chai');

const { expect } = chai;

chai.use(require('sinon-chai'));

const { Commands: { NetworkInfo, NetworkEntryReference } } = require('../../index');

const parameters = {
    psk: 'fake-psk',
    ssid: 'fake-network',
    identity: 'fake-identity',
    password: 'fake-password',
    keyManagement: 'fake-key-string'
};

describe('Commands/NetworkInfo', () => {
    describe('equals(another)', () => {
        it('Should be true if they are the exact same instance', () => {
            const subject = new NetworkInfo();
            expect(subject.equals(subject)).to.be.true;
        });

        it('Should be true if they have the same reference and same content', () => {
            const left = NetworkInfo.makeInstance({
                reference: new NetworkEntryReference(7), ...parameters
            });

            const right = NetworkInfo.makeInstance({
                reference: new NetworkEntryReference(7), ...parameters
            });

            expect(left.equals(right)).to.be.true;
        });

        it('Should be false if they are not the same entity', () => {
            const left = NetworkInfo.makeInstance({
                reference: new NetworkEntryReference(34), ...parameters
            });

            const right = NetworkInfo.makeInstance({
                reference: new NetworkEntryReference(41), ...parameters
            });

            expect(left.equals(right)).to.be.false;
        });

        it('Should be false if they don\'t have the same content', () => {
            const left = NetworkInfo.makeInstance({
                reference: new NetworkEntryReference(97), ...parameters, ssid: 'corrupt-value'
            });

            const right = NetworkInfo.makeInstance({
                reference: new NetworkEntryReference(97), ...parameters
            });

            expect(left.equals(right)).to.be.false;
        });
    });

    describe('hasTheSameContentsOf(another)', () => {
        it('Should be true if the objects are empty', () => {
            const left = NetworkInfo.makeInstance({ });
            const right = NetworkInfo.makeInstance({ });
            expect(left.hasTheSameContentsOf(right)).to.be.true;
        });
        it('Should be true if all attributes match', () => {
            const left = NetworkInfo.makeInstance(parameters);
            const right = NetworkInfo.makeInstance(parameters);
            expect(left.hasTheSameContentsOf(right)).to.be.true;
        });
        it('Should be false if SSIDs differ', () => {
            const left = NetworkInfo.makeInstance(parameters);
            const right = NetworkInfo.makeInstance({
                ...parameters, ssid: 'corrupt-value'
            });
            expect(left.hasTheSameContentsOf(right)).to.be.false;
        });
        it('Should be false if PSKs differ', () => {
            const left = NetworkInfo.makeInstance(parameters);
            const right = NetworkInfo.makeInstance({
                ...parameters, psk: 'corrupt-value'
            });
            expect(left.hasTheSameContentsOf(right)).to.be.false;
        });
        it('Should be false if KeyManagements differ', () => {
            const left = NetworkInfo.makeInstance(parameters);
            const right = NetworkInfo.makeInstance({
                ...parameters, keyManagement: 'corrupt-value'
            });
            expect(left.hasTheSameContentsOf(right)).to.be.false;
        });
        it('Should be false if identities differ', () => {
            const left = NetworkInfo.makeInstance(parameters);
            const right = NetworkInfo.makeInstance({
                ...parameters, identity: 'corrupt-value'
            });
            expect(left.hasTheSameContentsOf(right)).to.be.false;
        });
    });

    describe('isTheSameEntityAs(another)', () => {
        it('Should be true if they have equivalent references', () => {
            const left = new NetworkInfo(new NetworkEntryReference(13));
            const right = new NetworkInfo(new NetworkEntryReference(13));
            expect(left.isTheSameEntityAs(right)).to.be.true;
        });

        it('Should be false if they have mismatching references', () => {
            const left = new NetworkInfo(new NetworkEntryReference(11));
            const right = new NetworkInfo(new NetworkEntryReference(19));
            expect(left.isTheSameEntityAs(right)).to.be.false;
        });

        it('Should be false if either one is missing its reference', () => {
            const left = new NetworkInfo();
            const right = new NetworkInfo(new NetworkEntryReference(19));
            expect(left.isTheSameEntityAs(right)).to.be.false;
            expect(right.isTheSameEntityAs(left)).to.be.false;
        });

        it('Should be false if the other one is not a NetworkInfo instance', () =>{
            expect(new NetworkInfo().isTheSameEntityAs({ })).to.be.false;
        });
    });

    describe('*iterateSetPayloads()', () => {
        it('Should yield nothing if the object is empty', () => {
            const payloads = [ ...new NetworkInfo().iterateSetPayloads() ];
            expect(payloads).to.be.deep.equal([ ]);
        });
        it('Should yield all fields', () => {
            const reference = new NetworkEntryReference(101);
            const subject = NetworkInfo.makeInstance({ reference, ...parameters });
            expect([ ...subject.iterateSetPayloads() ]).to.be.deep.equal([
                'SET_NETWORK 101 ssid "fake-network"',
                'SET_NETWORK 101 psk "fake-psk"',
                'SET_NETWORK 101 key_mgmt "fake-key-string"',
                'SET_NETWORK 101 identity "fake-identity"',
                'SET_NETWORK 101 password "fake-password"'
            ]);
        });
    });
});