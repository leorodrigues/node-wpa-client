const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const {
    EnableNetworkEntry,
    NetworkEntryReference
} = require('../../src/commands');

const {
    fakeCommandTransceiver,
    fakeResponseParser
} = makeMocks(sinon.createSandbox());

describe('Commands/EnableNetworkEntry', () => {
    describe('async invoke()', () => {
        it('Should transmit "ENABLE_NETWORK 19", receive "OK" and return true.', async () => {
            fakeResponseParser.parseOk.returns('OK');
            fakeCommandTransceiver.transmit.resolves();
            fakeCommandTransceiver.receive.resolves('OK\n');

            const reference = new NetworkEntryReference(19);
            const subject = new EnableNetworkEntry(
                reference, fakeCommandTransceiver, fakeResponseParser);

            expect(await subject.invoke())
                .to.be.true;

            expect(fakeCommandTransceiver.transmit)
                .to.have.been.calledWith('ENABLE_NETWORK 19');

            expect(fakeCommandTransceiver.receive)
                .to.have.been.calledWith();
        });
    });
});