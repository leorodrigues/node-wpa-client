const chai = require('chai');

const { expect } = chai;

chai.use(require('sinon-chai'));

const { Commands: { PeerPinNegotiationMethod } } = require('../../index');

describe('Commands/PeerPinNegotiationMethod', () => {
    describe('.NONE#appendToPayload(payloadString)', () => {
        it('Should not change the payload at all', () =>
            expect(PeerPinNegotiationMethod.NONE.appendToPayload('fake-payload'))
                .to.be.equal('fake-payload'));
    });

    describe('.LABEL#appendToPayload(payloadString)', () => {
        it('Should append "label" to the end of the payload', () =>
            expect(PeerPinNegotiationMethod.LABEL.appendToPayload('fake-payload'))
                .to.be.equal('fake-payload label'));
    });

    describe('.DISPLAY#appendToPayload(payloadString)', () => {
        it('Should append "display" to the end of the payload', () =>
            expect(PeerPinNegotiationMethod.DISPLAY.appendToPayload('fake-payload'))
                .to.be.equal('fake-payload display'));
    });

    describe('.KEYPAD#appendToPayload(payloadString)', () => {
        it('Should add "keypad" to the end of the payload', () =>
            expect(PeerPinNegotiationMethod.KEYPAD.appendToPayload('fake-payload'))
                .to.be.equal('fake-payload keypad'));
    });
});