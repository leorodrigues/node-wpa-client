const chai = require('chai');
const { expect } = chai;

chai.use(require('sinon-chai'));

const { Commands } = require('../../index');

const subject = new Commands.CommandKit(
    'fake-message-transceiver',
    'fake-event-transceiver',
    'fake-event-registrar',
    'fake-response-parser');

describe('Commands/CommandKit', () => {
    describe('makeDisconnect()', () => {
        it('Should return a new "Disconnect" command instance.', () => {
            const command = subject.makeDisconnect();
            expect(command.constructor.name).to.be.equal('Disconnect');
            expect(command.commandTransceiver).to.be.equal('fake-message-transceiver');
            expect(command.responseParser).to.be.equal('fake-response-parser');
        });
    });

    describe('makeListNetworkEntries()', () => {
        it('Should return a new "ListNetworkEntries" command instance.', () => {
            const command = subject.makeListNetworkEntries();
            expect(command.constructor.name).to.be.equal('ListNetworkEntries');
            expect(command.commandTransceiver).to.be.equal('fake-message-transceiver');
            expect(command.responseParser).to.be.equal('fake-response-parser');
        });
    });

    describe('makeAddNetworkEntry()', () => {
        it('Should return a new "AddNetworkEntry" command instance.', () => {
            const command = subject.makeAddNetworkEntry();
            expect(command.constructor.name).to.be.equal('AddNetworkEntry');
            expect(command.commandTransceiver).to.be.equal('fake-message-transceiver');
            expect(command.responseParser).to.be.equal('fake-response-parser');
        });
    });

    describe('makePatchNetworkEntry(networkInfo)', () => {
        it('Should return a new "PatchNetworkEntry" command instance.', () => {
            const command = subject.makePatchNetworkEntry('fake-network-info');
            expect(command.constructor.name).to.be.equal('PatchNetworkEntry');
            expect(command.networkInfo).to.be.equal('fake-network-info');
            expect(command.commandTransceiver).to.be.equal('fake-message-transceiver');
            expect(command.responseParser).to.be.equal('fake-response-parser');
        });
    });

    describe('makeEnableNetworkEntry(networkEntryReference)', () => {
        it('Should return a new "EnableNetworkEntry" command instance.', () => {
            const command = subject.makeEnableNetworkEntry('fake-network-entry-reference');
            expect(command.constructor.name).to.be.equal('EnableNetworkEntry');
            expect(command.networkEntryReference).to.be.equal('fake-network-entry-reference');
            expect(command.commandTransceiver).to.be.equal('fake-message-transceiver');
            expect(command.responseParser).to.be.equal('fake-response-parser');
        });
    });

    describe('makeSelectNetworkEntry(networkEntryReference)', () => {
        it('Should return a new "SelectNetworkEntry" command instance.', () => {
            const command = subject.makeSelectNetworkEntry('fake-network-entry-reference');
            expect(command.constructor.name).to.be.equal('SelectNetworkEntry');
            expect(command.reference).to.be.equal('fake-network-entry-reference');
            expect(command.commandTransceiver).to.be.equal('fake-message-transceiver');
            expect(command.responseParser).to.be.equal('fake-response-parser');
        });
    });

    describe('makeDisableNetworkEntry(networkEntryReference)', () => {
        it('Should return a new "DisableNetworkEntry" command instance.', () => {
            const command = subject.makeDisableNetworkEntry('fake-network-entry-reference');
            expect(command.constructor.name).to.be.equal('DisableNetworkEntry');
            expect(command.networkEntryReference).to.be.equal('fake-network-entry-reference');
            expect(command.commandTransceiver).to.be.equal('fake-message-transceiver');
            expect(command.responseParser).to.be.equal('fake-response-parser');
        });
    });

    describe('makeGetNetworkInfo(networkEntryReference)', () => {
        it('Should return a new "GetNetworkInfo" command instance.', () => {
            const command = subject.makeGetNetworkInfo('fake-network-entry-reference');
            expect(command.constructor.name).to.be.equal('GetNetworkInfo');
            expect(command.networkEntryReference).to.be.equal('fake-network-entry-reference');
            expect(command.commandTransceiver).to.be.equal('fake-message-transceiver');
            expect(command.responseParser).to.be.equal('fake-response-parser');
        });
    });

    describe('makeGetScanResults()', () => {
        it('Should return a new "GetScanResults" command instance.', () => {
            const command = subject.makeGetScanResults();
            expect(command.constructor.name).to.be.equal('GetScanResults');
            expect(command.commandTransceiver).to.be.equal('fake-message-transceiver');
            expect(command.responseParser).to.be.equal('fake-response-parser');
        });
    });

    describe('makeGetStatus()', () => {
        it('Should return a new "GetStatus" command instance.', () => {
            const command = subject.makeGetStatus();
            expect(command.constructor.name).to.be.equal('GetStatus');
            expect(command.commandTransceiver).to.be.equal('fake-message-transceiver');
            expect(command.responseParser).to.be.equal('fake-response-parser');
        });
    });

    describe('makeListInterfaces()', () => {
        it('Should return a new "ListInterfaces" command instance.', () => {
            const command = subject.makeListInterfaces();
            expect(command.constructor.name).to.be.equal('ListInterfaces');
            expect(command.commandTransceiver).to.be.equal('fake-message-transceiver');
            expect(command.responseParser).to.be.equal('fake-response-parser');
        });
    });
});