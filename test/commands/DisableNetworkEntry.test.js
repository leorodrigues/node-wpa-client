const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const {
    DisableNetworkEntry,
    NetworkEntryReference
} = require('../../src/commands');

const {
    fakeCommandTransceiver,
    fakeResponseParser
} = makeMocks(sinon.createSandbox());

describe('Commands/DisableNetworkEntry', () => {
    describe('async invoke()', () => {
        it('Should transmit "DISABLE_NETWORK 17", receive "OK" and return true.', async () => {
            fakeResponseParser.parseOk.returns('OK');
            fakeCommandTransceiver.transmit.resolves();
            fakeCommandTransceiver.receive.resolves('OK\n');

            const reference = new NetworkEntryReference(17);
            const subject = new DisableNetworkEntry(
                reference, fakeCommandTransceiver, fakeResponseParser);

            expect(await subject.invoke())
                .to.be.true;

            expect(fakeCommandTransceiver.transmit)
                .to.be.calledWith('DISABLE_NETWORK 17');

            expect(fakeCommandTransceiver.receive)
                .to.be.calledWith();
        });
    });
});