const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const { Commands: { Disconnect } } = require('../../index');

const {
    fakeCommandTransceiver,
    fakeResponseParser
} = makeMocks(sinon.createSandbox());

const subject = new Disconnect(fakeCommandTransceiver, fakeResponseParser);

describe('Commands/Disconnect', () => {
    describe('async invoke()', () => {
        it('Should successfully fire up the command', async () => {
            fakeResponseParser.parseOk.returns('OK');
            fakeCommandTransceiver.transmit.resolves();
            fakeCommandTransceiver.receive.resolves('fake-reply');

            expect(await subject.invoke()).to.be.true;

            expect(fakeCommandTransceiver.transmit)
                .to.have.been.calledWith('DISCONNECT');

            expect(fakeCommandTransceiver.receive)
                .to.be.calledOnceWith();

            expect(fakeResponseParser.parseOk)
                .to.be.calledOnceWith('fake-reply');
        });
    });
});