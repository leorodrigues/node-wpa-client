const chai = require('chai');

const { expect } = chai;

chai.use(require('sinon-chai'));

const { Commands: { NetworkEntryReference } } = require('../../index');


describe('Commands/NetworkEntryReference', () => {

    describe('get enableNetworkCmdString()', () => {
        it('Should be "ENABLE_NETWORK 17".', async () => {
            const reference = new NetworkEntryReference(17);
            expect(reference.enableNetworkCmdString).to.be.equal('ENABLE_NETWORK 17');
        });
    });

    describe('get disableNetworkCmdString()', () => {
        it('Should be "DISABLE_NETWORK 19".', async () => {
            const reference = new NetworkEntryReference(19);
            expect(reference.disableNetworkCmdString).to.be.equal('DISABLE_NETWORK 19');
        });
    });

    describe('get selectNetworkCmdString()', () => {
        it('Should be "SELECT_NETWORK 11".', async () => {
            const reference = new NetworkEntryReference(11);
            expect(reference.selectNetworkCmdString).to.be.equal('SELECT_NETWORK 11');
        });
    });

    describe('get removeNetworkCmdString()', () => {
        it('Should be "REMOVE_NETWORK 34".', async () => {
            const reference = new NetworkEntryReference(34);
            expect(reference.removeNetworkCmdString).to.be.equal('REMOVE_NETWORK 34');
        });
    });


    describe('equals(another)', () => {
        it('Should be true if the ids are the same.', async () => {
            const leftSideReference = new NetworkEntryReference(13);
            const rightSideReference = new NetworkEntryReference(13);
            expect(leftSideReference.equals(rightSideReference)).to.be.true;
        });

        it('Should be false if the ids are different', async () => {
            const leftSideReference = new NetworkEntryReference(23);
            const rightSideReference = new NetworkEntryReference(7);
            expect(leftSideReference.equals(rightSideReference)).to.be.false;
        });
    });

    describe('interpolate(text)', () => {
        it('Should replace the placeholder with its own id.', () => {
            const msg = new NetworkEntryReference(42).interpolate('yada yada yada :id yada yada');
            expect(msg).to.be.equal('yada yada yada 42 yada yada');
        });
    });
});