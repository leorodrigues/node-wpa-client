const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const { Commands: { MessageCommand, IncorrectProtocolError } } = require('../../index');

const { fakeCommandTransceiver, sandbox } = makeMocks(sinon.createSandbox());

const subject = new MessageCommand(fakeCommandTransceiver);

describe('Commands/MessageCommand', () => {
    afterEach(() => sandbox.reset());

    describe('async transmit(message)', () => {

        it('Should delegate to the transceiver', async () => {
            fakeCommandTransceiver.transmit.resolves('fake-void-result');

            expect(await subject.transmit('fake-message'))
                .to.be.equal('fake-void-result');

            expect(fakeCommandTransceiver.transmit)
                .to.have.been.calledWithExactly('fake-message');
        });
    });

    describe('async receive()', () => {

        it('Should delegate to the transceiver', async () => {
            fakeCommandTransceiver.receive.resolves('fake-reply');

            expect(await subject.receive()).to.be.equal('fake-reply');

            expect(fakeCommandTransceiver.receive)
                .to.have.been.calledWithExactly();
        });
    });

    describe('confirmReply(actualReply, expectedReply)', () => {

        it('Should passthrough if the arguments match', async () => {
            subject.confirmReply('tomato', 'tomato');
        });

        it('Should throw if the arguments don\'t match', async () => {
            try {
                subject.confirmReply('potato', 'refrigerator');

                /* istanbul ignore next */
                throw new Error('Excecution should not reach this point.');
            } catch(error) {
                expect(error).to.be.instanceOf(IncorrectProtocolError);
            }
        });
    });
});