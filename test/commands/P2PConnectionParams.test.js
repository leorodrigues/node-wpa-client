
const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const { Commands: { P2PConnectionParams } } = require('../../index');

const sandbox = sinon.createSandbox();

const fakePeerAddress = {
    appendToPayload: sandbox.stub()
};

const fakeProvisioningMethod = {
    appendToPayload: sandbox.stub()
};

const fakeGroupFormationDirective = {
    appendToPayload: sandbox.stub()
};

const subject = new P2PConnectionParams(
    fakePeerAddress,
    fakeProvisioningMethod,
    fakeGroupFormationDirective);

describe('Commands/P2PConnectionParams', () => {
    describe('appendToPayload(payloadString)', () => {
        it('Should return a fully parameterized payload', async () => {
            fakePeerAddress.appendToPayload.returns('fake-address');
            fakeProvisioningMethod.appendToPayload.returns('fake-method');
            fakeGroupFormationDirective.appendToPayload.returns('fake-directive');

            const result = subject.appendToPayload('fake-payload');

            expect(result).to.be.equal('fake-directive');

            expect(fakePeerAddress.appendToPayload)
                .to.have.been.calledOnceWithExactly('fake-payload');

            expect(fakeProvisioningMethod.appendToPayload)
                .to.have.been.calledOnceWithExactly('fake-address');

            expect(fakeGroupFormationDirective.appendToPayload)
                .to.have.been.calledOnceWithExactly('fake-method');
        });
    });
});