
module.exports = function makeMocks(sandbox) {
    const fakeCommandTransceiver = {
        transmit: sandbox.stub(),
        connect: sandbox.stub(),
        receive: sandbox.stub(),
        close: sandbox.stub(),
        bind: sandbox.stub()
    };

    const fakeEventTransceiver = {
        transmit: sandbox.stub()
    };

    const fakeResponseParser = {
        parseListInterfaces: sandbox.stub(),
        parseNetworkEntries: sandbox.stub(),
        parseScanResult: sandbox.stub(),
        parseStatus: sandbox.stub(),
        parsePong: sandbox.stub(),
        parseOk: sandbox.stub()
    };

    return {
        fakeCommandTransceiver,
        fakeEventTransceiver,
        fakeResponseParser,
        sandbox
    };
};