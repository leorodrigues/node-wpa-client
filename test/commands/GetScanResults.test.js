const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('./Mockery');

const {
    fakeCommandTransceiver,
    fakeResponseParser
} = makeMocks(sinon.createSandbox());

const { GetScanResults } = require('../../src/commands');

describe('Commands/GetScanResults', () => {
    describe('async invoke()', () => {

        it('Should successfully fire up the command.', async () => {
            fakeCommandTransceiver.transmit.resolves();
            fakeCommandTransceiver.receive.resolves('fake-reply');
            fakeResponseParser.parseScanResult.returns('scan-results');

            const subject = new GetScanResults(
                fakeCommandTransceiver, fakeResponseParser);

            expect(await subject.invoke()).to.be.equal('scan-results');

            expect(fakeCommandTransceiver.transmit)
                .to.have.been.calledOnceWithExactly('SCAN_RESULTS');

            expect(fakeCommandTransceiver.receive)
                .to.have.been.calledOnceWithExactly();

            expect(fakeResponseParser.parseScanResult)
                .to.have.been.calledOnceWithExactly('fake-reply');
        });
    });
});