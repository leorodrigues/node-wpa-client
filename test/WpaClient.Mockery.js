
function makeMocks(sandbox) {

    const fakePathResolver = {
        getServerSocketPath: sandbox.stub(),
        getControlSocketPath: sandbox.stub()
    };

    const fakeCommandKit = {
        makeOpenCommandTransceiver: sandbox.stub(),
        makeOpenMonitorTransceiver: sandbox.stub(),
        makeDisableNetworkEntry: sandbox.stub(),
        makeListNetworkEntries: sandbox.stub(),
        makeSelectNetworkEntry: sandbox.stub(),
        makeEnableNetworkEntry: sandbox.stub(),
        makePatchNetworkEntry: sandbox.stub(),
        makeAddNetworkEntry: sandbox.stub(),
        makeListInterfaces: sandbox.stub(),
        makeGetNetworkInfo: sandbox.stub(),
        makeGetScanResults: sandbox.stub(),
        makeDisconnect: sandbox.stub(),
        makeGetStatus: sandbox.stub(),
        makeAttach: sandbox.stub()
    };

    return { fakePathResolver, fakeCommandKit, sandbox };
}

module.exports = makeMocks;