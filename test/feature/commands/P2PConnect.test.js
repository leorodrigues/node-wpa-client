
const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('../../commands/Mockery');

const { Commands: { CommandKit } } = require('../../../index');

const { fakeEventTransceiver, sandbox } = makeMocks(sinon.createSandbox());

const kit = new CommandKit(
    undefined, fakeEventTransceiver, undefined, undefined);

const fakeParameters = {
    appendToPayload: sandbox.stub()
};

const subject = kit.makeP2PConnect(fakeParameters);

describe('Feature:Commands/P2PConnect', () => {
    it('Should successfully fire up the command', async () => {
        fakeParameters.appendToPayload.returns('parameterized-payload');
        fakeEventTransceiver.transmit.resolves();

        await subject.invoke();

        expect(fakeParameters.appendToPayload)
            .to.have.been.calledOnceWithExactly('P2P_CONNECT');
        expect(fakeEventTransceiver.transmit)
            .to.have.been.calledOnceWithExactly('parameterized-payload');
    });
});