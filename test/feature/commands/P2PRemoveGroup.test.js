
const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('../../commands/Mockery');

const { Commands: { CommandKit } } = require('../../../index');

const { fakeEventTransceiver } = makeMocks(sinon.createSandbox());

const kit = new CommandKit(
    undefined, fakeEventTransceiver, undefined, undefined);

const subject = kit.makeP2PRemoveGroup();

describe('Feature:Commands/P2PRemoveGroup', () => {
    it('Should successfully fire up the command', async () => {
        await subject.invoke();

        expect(fakeEventTransceiver.transmit)
            .to.have.been.calledOnceWithExactly('P2P_GROUP_REMOVE');
    });
});