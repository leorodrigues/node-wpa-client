
const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('../../commands/Mockery');

const { Commands: { CommandKit } } = require('../../../index');

const { fakeEventTransceiver, sandbox } = makeMocks(sinon.createSandbox());

const kit = new CommandKit(
    undefined, fakeEventTransceiver, undefined, undefined);

const fakePeriod = {
    appendToPayload: sandbox.stub()
};

const subject = kit.makeP2PListen(fakePeriod);

describe('Feature:Commands/P2PListen', () => {
    it('Should successfully fire up the command', async () => {
        fakePeriod.appendToPayload.returns('fake-payload-with-period');

        await subject.invoke();

        expect(fakePeriod.appendToPayload)
            .to.have.been.calledOnceWithExactly('P2P_LISTEN');

        expect(fakeEventTransceiver.transmit)
            .to.have.been.calledOnceWithExactly('fake-payload-with-period');
    });
});