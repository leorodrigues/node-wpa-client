
const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('../../commands/Mockery');

const { Commands: { CommandKit } } = require('../../../index');

const { fakeEventTransceiver } = makeMocks(sinon.createSandbox());

const kit = new CommandKit(
    undefined, fakeEventTransceiver, undefined, undefined);

const subject = kit.makeP2PAddGroup();

describe('Feature:Commands/P2PAddGroup', () => {
    it('Should successfully fire up the command', async () => {
        fakeEventTransceiver.transmit.resolves();

        await subject.invoke();

        expect(fakeEventTransceiver.transmit)
            .to.have.been.calledWith('P2P_GROUP_ADD');
    });
});