
const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const makeMocks = require('../../commands/Mockery');

const { Commands: { CommandKit } } = require('../../../index');

const { fakeEventTransceiver, sandbox } = makeMocks(sinon.createSandbox());

const kit = new CommandKit(
    undefined, fakeEventTransceiver, undefined, undefined);

const fakePeerAddress = {
    appendToPayload: sandbox.stub()
};

const fakePeerPinNegotiationMethod = {
    appendToPayload: sandbox.stub()
};

const subject = kit.makeP2PRequestProvisionDiscovery(
    fakePeerAddress, fakePeerPinNegotiationMethod);

describe('Feature:Commands/P2PRequestProvisionDiscovery', () => {
    it('Should successfully fire up the command', async () => {
        fakePeerPinNegotiationMethod.appendToPayload.returns('fake-payload-with-method');
        fakePeerAddress.appendToPayload.returns('fake-payload-with-address');

        await subject.invoke();

        expect(fakePeerAddress.appendToPayload)
            .to.have.been.calledOnceWithExactly('P2P_PROV_DISC');

        expect(fakePeerPinNegotiationMethod.appendToPayload)
            .to.have.been.calledOnceWithExactly('fake-payload-with-address');

        expect(fakeEventTransceiver.transmit)
            .to.have.been.calledOnceWithExactly('fake-payload-with-method');
    });
});