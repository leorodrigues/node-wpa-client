const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;

chai.use(require('sinon-chai'));

const { WpaClient } = require('../index');

const makeMocks = require('./WpaClient.Mockery');

const {
    sandbox,
    fakeCommandKit,
    fakePathResolver,
} = makeMocks(sinon.createSandbox());

const subject = new WpaClient(fakePathResolver, fakeCommandKit);

const fakeCommand = { invoke: sandbox.stub() };

describe('WpaClient', () => {
    afterEach(() => sandbox.reset());

    describe('async open()', () => {
        it('Should delegate to the `Open` and `Attach` commands', async () => {
            const fakeOpen = { invoke: sandbox.stub() };
            const fakeAttach = { invoke: sandbox.stub() };

            fakePathResolver.getServerSocketPath.returns('server.sock');
            fakePathResolver.getControlSocketPath.returns('client.sock');
            fakeCommandKit.makeOpenCommandTransceiver.returns(fakeOpen);
            fakeCommandKit.makeOpenMonitorTransceiver.returns(fakeOpen);
            fakeCommandKit.makeAttach.returns(fakeAttach);
            fakeOpen.invoke.resolves();
            fakeAttach.invoke.resolves();

            await subject.open();

            expect(fakePathResolver.getServerSocketPath).to.have.been.called;
            expect(fakePathResolver.getControlSocketPath)
                .to.have.been.calledTwice
                .to.have.been.calledWith('1')
                .to.have.been.calledWith('1');

            expect(fakeCommandKit.makeOpenCommandTransceiver)
                .to.have.been.calledWith('server.sock', 'client.sock');

            expect(fakeCommandKit.makeOpenMonitorTransceiver)
                .to.have.been.calledWith('server.sock', 'client.sock');

            expect(fakeOpen.invoke).to.have.been.calledWith();
            expect(fakeAttach.invoke).to.have.been.calledWith();
        });
    });

    describe('async listNetworkEntries()', () => {
        it('Should fabricate a "ListNetworkEntries" command and delegate to it.', async () => {
            fakeCommandKit.makeListNetworkEntries.returns(fakeCommand);
            fakeCommand.invoke.resolves('fake-list');
            expect(await subject.listNetworkEntries()).to.be.equal('fake-list');
        });
    });

    describe('async addNetworkEntry()', () => {
        it('Should fabricate an "AddNetworkEntry" command and delegate to it.', async () => {
            fakeCommandKit.makeAddNetworkEntry.returns(fakeCommand);
            fakeCommand.invoke.resolves('fake-reference');
            expect(await subject.addNetworkEntry()).to.be.equal('fake-reference');
        });
    });

    describe('async disconnect()', () => {
        it('Should fabricate a "Disconnect" command and delegate to it.', async () => {
            fakeCommandKit.makeDisconnect.returns(fakeCommand);
            fakeCommand.invoke.resolves('fake-result');
            expect(await subject.disconnect()).to.be.equal('fake-result');
        });
    });

    describe('async patchNetworkEntry(networkInfo)', () => {
        it('Should fabricate a "PatchNetworkEntry" command and delegate to it.', async () => {
            fakeCommandKit.makePatchNetworkEntry.returns(fakeCommand);
            fakeCommand.invoke.resolves('fake-result');
            expect(await subject.patchNetworkEntry('fake-info')).to.be.equal('fake-result');
            expect(fakeCommandKit.makePatchNetworkEntry).to.have.been.calledOnceWith('fake-info');
        });
    });

    describe('async selectNetworkEntry(networkEntryReference)', () => {
        it('Should fabricate a "SelectNetworkEntry" command and delegate to it.', async () => {
            fakeCommandKit.makeSelectNetworkEntry.returns(fakeCommand);
            fakeCommand.invoke.resolves('fake-result');
            expect(await subject.selectNetworkEntry('fake-ref')).to.be.equal('fake-result');
            expect(fakeCommandKit.makeSelectNetworkEntry).to.have.been.calledOnceWith('fake-ref');
        });
    });

    describe('async enableNetworkEntry(networkEntryReference)', () => {
        it('Should fabricate an "EnableNetworkEntry" command and delegate to it.', async () => {
            fakeCommandKit.makeEnableNetworkEntry.returns(fakeCommand);
            fakeCommand.invoke.resolves('fake-result');
            expect(await subject.enableNetworkEntry('fake-ref')).to.be.equal('fake-result');
            expect(fakeCommandKit.makeEnableNetworkEntry).to.have.been.calledOnceWith('fake-ref');
        });
    });

    describe('async disableNetworkEntry(networkEntryReference)', () => {
        it('Should fabricate a "DisableNetworkEntry" command and delegate to it.', async () => {
            fakeCommandKit.makeDisableNetworkEntry.returns(fakeCommand);
            fakeCommand.invoke.resolves('fake-result');
            expect(await subject.disableNetworkEntry('fake-ref')).to.be.equal('fake-result');
            expect(fakeCommandKit.makeDisableNetworkEntry).to.have.been.calledOnceWith('fake-ref');
        });
    });

    describe('async getNetworkInfo(networkEntryReference)', () => {
        it('Should fabricate a "GetScanResults" command and delegate to it.', async () => {
            fakeCommandKit.makeGetScanResults.returns(fakeCommand);
            fakeCommand.invoke.resolves('fake-result');
            expect(await subject.getScanResults()).to.be.equal('fake-result');
            expect(fakeCommandKit.makeGetScanResults)
                .to.have.been.calledOnceWithExactly();
        });
    });

    describe('async getNetworkInfo(networkEntryReference)', () => {
        it('Should fabricate a "GetNetworkInfo" command and delegate to it.', async () => {
            fakeCommandKit.makeGetNetworkInfo.returns(fakeCommand);
            fakeCommand.invoke.resolves('fake-result');
            expect(await subject.getNetworkInfo('fake-ref')).to.be.equal('fake-result');
            expect(fakeCommandKit.makeGetNetworkInfo).to.have.been.calledOnceWith('fake-ref');
        });
    });

    describe('async getStatus()', () => {
        it('Should fabricate a "GetStatus" command and delegate to it.', async () => {
            fakeCommandKit.makeGetStatus.returns(fakeCommand);
            fakeCommand.invoke.resolves('fake-result');
            expect(await subject.getStatus()).to.be.equal('fake-result');
            expect(fakeCommandKit.makeGetStatus).to.have.been.calledOnceWithExactly();
        });
    });

    describe('async listInterfaces()', () => {
        it('Should fabricate a "ListInterfaces" command and delegate to it.', async () => {
            fakeCommandKit.makeListInterfaces.returns(fakeCommand);
            fakeCommand.invoke.resolves('fake-list');
            expect(await subject.listInterfaces()).to.be.equal('fake-list');
            expect(fakeCommandKit.makeListInterfaces).to.have.been.calledOnceWithExactly();
        });
    });
});