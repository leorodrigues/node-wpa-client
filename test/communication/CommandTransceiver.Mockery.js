
const fs = require('fs');
const unix = require('unix-dgram');

class MessageHandler {
    constructor(socket, sessionId) {
        this.responses = { };
        this.socket = socket;
        this.sessionId = sessionId;
    }

    getControlSocketPath(localId = '0') {
        return `/tmp/test-client-${this.sessionId}-${localId}.sock`;
    }

    listenForMessages() {
        this.socket.on('message', data => {
            this.reply(data);
        });
    }

    reply(data) {
        return new Promise((resolve, reject) => {
            const message = data.toString();
            const reply = this.doReplyFor(message);

            const handleError = error => {
                this.socket.removeListener('error', handleError);
                reject(error);
            };

            const handleSuccess = () => {
                this.socket.removeListener('error', handleError);
                this.socket.removeListener('writable', handleSuccess);
                resolve({ data, reply });
            };

            this.socket.once('writable', handleSuccess);
            this.socket.once('error', handleError);
            this.socket.sendto(reply, 0, reply.length, this.getControlSocketPath());
        });
    }

    doReplyFor(message) {
        const reply = this.responses[message];
        if (reply.data)
            return Buffer.from(reply.data);
        if (reply.type) {
            const Type = reply.type;
            throw new Type(...(reply.params || [ ]));
        }
        throw new Error('unexpected message');
    }

    clearResponses() {
        this.responses = { };
    }

    respond(message, data) {
        this.responses[message] = { data };
    }

    throw(message, errorClass, ...params) {
        this.responses[message] = { errorClass, params };
    }

    relinquishSocket() {
        if (fs.existsSync(this.controlSocketPath))
            fs.unlinkSync(this.controlSocketPath);
    }
}

class FakeServer {
    constructor() {
        this.socket = unix.createSocket('unix_dgram');
        this.sessionId = (new Date()).getTime().toString(16);
        this.messageHandler = new MessageHandler(this.socket, this.sessionId);
    }

    get serverSocketPath() {
        return `/tmp/test-server-${this.sessionId}.sock`;
    }

    get clientSocketPath() {
        return this.messageHandler.getControlSocketPath();
    }

    bind() {
        return new Promise((resolve, reject) => {
            const handleError = error => {
                this.socket.removeListener('error', handleError);
                reject(error);
            };

            this.socket.once('listening', () => {
                this.socket.removeListener('error', handleError);
                this.messageHandler.listenForMessages();
                resolve(this);
            });

            this.socket.once('error', handleError);
            this.socket.bind(this.serverSocketPath);
        });
    }

    relinquishSocket() {
        if (fs.existsSync(this.serverSocketPath))
            fs.unlinkSync(this.serverSocketPath);
        if (fs.existsSync(this.clientSocketPath))
            fs.unlinkSync(this.clientSocketPath);
        this.messageHandler.relinquishSocket();
    }

    clearResponses() {
        this.messageHandler.clearResponses();
    }

    respond(message, data) {
        this.messageHandler.respond(message, data);
    }

    throw(message, errorClass, ...params) {
        this.messageHandler.throw(message, errorClass, ...params);
    }

    close() {
        this.socket.close();
        this.relinquishSocket();
    }
}

module.exports = { FakeServer };