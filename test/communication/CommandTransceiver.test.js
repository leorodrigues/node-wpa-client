const chai = require('chai');

const { expect } = chai;

const { FakeServer } = require('./CommandTransceiver.Mockery');
const fakeServer = new FakeServer();

const { Communication } = require('../../index');
const subject = new Communication.TransceiverKit().makeCommandTransceiver();

describe('Communication/CommandTransceiver', () => {
    describe('Given that the environment fulfills the requirements', () => {
        before(async () => {
            await fakeServer.bind();
            await subject.connect(fakeServer.serverSocketPath);
            await subject.bind(fakeServer.clientSocketPath);
        });
    
        after(async () => {
            await subject.close();
            await fakeServer.close();
        });

        afterEach(async () => {
            fakeServer.clearResponses();
        });
    
        it('Should send a message and receive a reply', async () => {
            fakeServer.respond('HELLO', 'WORLD');
            await subject.transmit('HELLO');
            const reply = await new Promise((resolve) => {
                const run = () => subject.receive().then(resolve);
                setTimeout(run, 1000);
            });
            expect(reply).to.be.equal('WORLD');
        });
    
        it('Should register for receive first', async () => {
            fakeServer.respond('PING', 'PONG');
            const promise = subject.receive();
            await subject.transmit('PING');
            expect(await promise).to.be.equal('PONG');
        });
    });

    describe('Given there is no server available', () => {
        it('Should fail during connect', async () => {
            try {
                await subject.connect('./non-existent.sock');
            } catch(error) {
                expect(error).to.be.instanceOf(Error);
            }
        });
    });

    describe('Given the socket file is impossible to create', () => {
        it('Should fail during bind', async () => {
            try {
                await subject.bind('./non-existent-path/client.sock');
            } catch(error) {
                expect(error).to.be.instanceOf(Error);
            }
        });
    });
});